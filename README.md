Installation
=====================================

febrl-0.4.2 installation instructions
--------------------------

Peter Christen, 14 December 2011


The basic Febrl-0.4.2 installation requires Python 2.6 or 2.7, with
additional modules and libraries needed for the graphical user interface
(GUI) and the support vector machine (SVM) classification method.

Python:
http://www.python.org

For the GUI, PyGTK and Matplotlib are required:
http://www.pygtk.org
http://matplotlib.sourceforge.net

The SVM classifier is based on libsvm:
http://www.csie.ntu.edu.tw/~cjlin/libsvm/

Follow the installation instruction within the python directory once
you have downloaded and unpacked libsvm.


Test of installed modules:
--------------------------

To test if the required libraries are installed in your Python
distribution, start Python and try the following:

>>> import matplotlib
>>> matplotlib.use('GTK')

>>> import pygtk
>>> pygtk.require("2.0")

>>> import gtk
>>> import gtk.glade

>>> import svm

None of these import commands should give you an error.


Febrl-0.4.2 installation:
-------------------------

Unpack the febrl-0.4.2.tar.gz or febrl-0.4.2.zip archive and a new
directory named 'febrl-0.4.2' will be created containing all the
necessary Febrl modules and additional files such as example data sets,
documentation, the Febrl data set generator and testing programs.

Go into the 'tests' sub-directory within 'febrl-0.4.2' and either run
all tests using the corresponding script provided:

#> ./allTests.sh

or run the tests individually, for examples:

#> python indexingTest.py
#> python datasetTest.py

Note (by Van Ly, FSF): Due to differences in the end of line convention
for Mac (CR or \r), for Linux (LF or \n) and for Windows (CRLF or \r\n),
allTests.sh currently has the Windows end of line convention which
misbehaves on Linux Fedora 9 (in VirtualBox on Windows). Thus, the test
script does not execute properly, the work around I used was to apply

#> cat allTests.sh |sed -e ',\r,,' > allTests_1.sh
#> chmod u+x allTests_1.sh ; ./allTests_1.sh


Starting Febrl-0.4.2
--------------------

The Febrl GUI can be started using:

#> ./guiFebrl.py

(you might have to set the execute permission for the file guiFebrl.py),
or using:

#> python guiFebrl.py

Traditional Febrl project modules (either implemented manually or
saved from within the GUI) can be run outside the GUI, for example:

#> python my-linkage-project.py


Problems and errors:
--------------------

Please note that this is the initial 'alpha' distribution of Febrl-0.4.2
which has only been tested to a limited extent on an Ubuntu Linux platform
(specifically Ubuntu with Python 2.6 and Python 2.7).

Please report any problems and bugs to: peter.christen@anu.edu.au


Febrl-0.4.2 mailing lists:
--------------------------

To receive updates and news on Febrl please subscribe to the
announcement mailing list at:

https://sourceforge.net/projects/febrl/

Usage
=====================================
To get a rule-based classification

1. First build a Febrl Deduplicate Project

2. Import the necessary modules at the top of the Febrl project
<code>
import provenance
import rule_based_classification
import evaluation
import rules
</code>

3. Initialize an evaluator after <code>index.compact()</code> using:
    <code>
    evaluator = evaluation.Evaluation(data_set_a)
    evaluator.build('id')
    </code>

4. Initialize an rule object by add:
    <code>
    rules = rules.Rules(file_name="your_rules.csv")
    </code>


5. A rule_list should be defined, which contains a list of rule ids:
    <code>
    rule_list = [rule_id]
    </code>

    You can also build new rule and get the id of the rule:
    <code>
    rule1 = rules.add_rule(rule,rule_type,rule_weight,dataset)
    <code>

    A rule is a list of condition with the relation of conjunction, for example:
    > "(0,"<", 1),(4,">=",0.2)" 
    means the similarity of first comparision filed must be smaller than 1 AND the similarity of the fifth comparision field should be no less than 0.2
6. A rule-based classifier is than built by using:
<code>
classifier = rule_based_classification.RuleBased(rule_list = rule_list,evaluator = evaluator, rules = rules, filename= 'classified')
</code>

The classified result will be store into the file. 

To set up a ER tree and repairing the result:

1. Import the necessary modules

2. Define the data set by copy the code for the Febrl project
<code>
data_set_a = dataset.DataSetCSV(description.....
</code>

3. Set up a evalutor
<code>
evaluator = evaluation.Evaluation(data_set_a)
evaluator.build('ground_truth_field')
</code>

4. Set up a ER tree clustering
<code>
ER_tree = provenance.ProvenanceGraph(evaluator = evaluator,filename = 'classfied_file',num_vectors,num_repair_per_iterator)
ER_tree.build_clusters()
ER_tree.clustering()
</code>

5. Repair ER rusult
<code>
provenance_graph.repair_result()
</code>

