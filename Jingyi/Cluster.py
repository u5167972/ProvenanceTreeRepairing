#import graph_tool.all as gt
import operator

class Cluster:

# =============================================================================
  """ A struct of Edge
    node_list: a list of node in the cluster
    edge_dict: a dictory of edge in the cluster
    hard_edges: a dictory of initial hard edge 
    graph: clustering graph
  """

  def __init__(self,rec):
    #Initial graph tool 
    '''
    self.g = gt.Graph(directed=False)
    self.output ="test.pdf"
    #set different edge color for different type of rules
    self.edge_color = self.g.new_edge_property('vector<double>')
    self.g.edge_properties['edge_color'] = self.edge_color
    self.color_map = {'hard+':(1,0,0,1),'soft+':(0.8,0.3,0,0.6),'hard-':(0,0,1,1),'soft-':(0,0.3,0.8,0.6)}
    self.edge_weight =self.g.new_edge_property('double')
    self.g.edge_properties['edge_weigth'] = self.edge_weight

    #set new edge_property label

    self.edge_label = self.g.new_edge_property("string")
    self.node_lable = self.g.new_vertex_property("string")
    '''
    if "&" in rec:
      self.node_list = rec.split("&")
    else:
      self.node_list = [rec]
    self.edge_label = []
    self.clean_list =[]
    self.neg_count = []
    self.ng = []
    self.hard_edges = []
    self.root = None
    self.remove_node = []
    # ---------------------------------------------------------------------------

  def insert(self,rec_tuple,record):
    if '&' not in record and record not in self.node_list:
      self.node_list.append(record)
    elif '&' in record and record not in self.ng:
      for integrated_node in self.ng:
        if record in integrated_node:
          #print("record included",record,integrated_node)
          break
        if integrated_node in record:
          self.ng.remove(integrated_node)
      lst = record.split("&")
      self.ng.append(record)
      for n in lst:
        if n not in self.node_list:
          self.node_list.append(n)    

    if rec_tuple not in self.edge_label and (rec_tuple[1],rec_tuple[0]) not in self.edge_label:
      self.edge_label.append(rec_tuple)
  # ---------------------------------------------------------------------------
  def insert_edge(self,rec_tuple):
    if rec_tuple not in self.edge_label and (rec_tuple[1],rec_tuple[0]) not in self.edge_label:
      self.edge_label.append(rec_tuple)

  # ---------------------------------------------------------------------------
  def insert_node(self,record):

    if '&' not in record and record not in self.node_list:
      self.node_list.append(record)
    elif '&' in record and record not in self.ng:
      for integrated_node in self.ng:
        if record in integrated_node:
          record = integrated_node
          #return
          break
        if integrated_node in record:
          self.ng.remove(integrated_node)
      lst = record.split("&")
      self.ng.append(record)
      for n in lst:
        if n not in self.node_list:
          self.node_list.append(n)

  #---------------------------------------------------------------------------
  def remove(self,node):
    if node in self.node_list:
      self.node_list.remove(node)
    elif node in self.ng:
      self.ng.remove(node)
      for n in node.split("&"):
        self.node_list.remove(n)
    for nodes in self.ng:
      if node in nodes.split("&"):
        self.ng.remove(nodes)
        node = nodes
    self.remove_node.append(node)
    return node

  # ---------------------------------------------------------------------------
  def remove_edge(self,node_pair):
    if node_pair in self.edge_label:
      self.edge_label.remove(node_pair)
      return True
    elif (node_pair[1],node_pair[0]) in self.edge_label:
      self.edge_label.remove((node_pair[1],node_pair[0]))
      return True
    else:
      return False
  # ---------------------------------------------------------------------------

  def merge(self,rec_tuple):

    for rec in rec_tuple:
      if '&'not in rec and rec not in self.node_list:
        self.node_list.append(rec)
      elif '&' in rec and rec not in self.ng:
        for integrated_node in self.ng:
          if rec in integrated_node:
            #print("222record included",rec,integrated_node)
            rec = integrated_node
            break
          if integrated_node in rec:
            self.ng.remove(integrated_node)
        lst = rec.split("&")
        self.ng.append(rec)

        for n in lst:
          if n not in self.node_list:
            self.node_list.append(n)

    if rec_tuple not in self.edge_label and (rec_tuple[1],rec_tuple[0]) not in self.edge_label:
      self.edge_label.append(rec_tuple)
  # ---------------------------------------------------------------------------
  def negative_edge(self,neg_tuple):
    if neg_tuple not in self.clean_list and (neg_tuple[1],neg_tuple[0]) not in self.clean_list:
      self.clean_list.append(neg_tuple)
      for node in neg_tuple:
        if node in self.neg_count:
          self.neg_count[node] += 1
        else:
          self.neg_count[node] = 1
  # ---------------------------------------------------------------------------
  def clean(self):
    pos_list = []
    edge_label = self.edge_label[:]
    for edge_tuple in edge_label:
      if edge_tuple[0] in self.remove_node and edge_tuple[1] in self.remove_node:
        pos_list.append(edge_tuple)
      if edge_tuple[0] in self.remove_node or edge_tuple[1] in self.remove_node:
        self.edge_label.remove(edge_tuple)
    return pos_list
  # ---------------------------------------------------------------------------
  def find_node(self,rec_list):
    edge = self.root
    if edge.type == 'node':
      return rec_list,edge,None
    while 1:

      #print(edge.children)
      try:
        lchild = edge.children[0]
        #lchild[1].parent = rchild[1].parent
        #print(lchild[1].parent)
        #print(rchild[1].parent)
      except IndexError:
        #print(edge.children)
        return rec_list,edge,None
      try:
        rchild = edge.children[1]
      except IndexError:
        #print(edge.children)
        return rec_list,edge,None

      if set(rec_list) == set(lchild[0]):
        #print("parent",edge, lchild[1].parent[0])
        return lchild[0],edge,lchild[1].parent[1]

      elif set(rec_list) == set(rchild[0]):
        #print("parent",edge, lchild[1].parent[0])

        return rchild[0],edge,rchild[1].parent[1]

      elif set(rec_list) == set(rec_list).intersection(set(lchild[0])):
        #the rec list in the list of node of left child

        edge = lchild[1]
      elif set(rec_list) == set(rec_list).intersection(set(rchild[0])):
        edge = rchild[1]
      else:

        return rec_list,edge,None
  # ---------------------------------------------------------------------------

  def get_size(self):
    """Get size of the cluster
    """
    return len(self.node_list)
    # ---------------------------------------------------------------------------
  def draw_graph(self):
    """Print out graph using graph-tool
    """
    logging.info('Drawing graph....')
    logging.info('Total vertices %d',self.g.num_vertices())
    logging.info('Number of edges: %d', self.g.num_edges())
    '''
    pos = gt.sfdp_layout(self.g)
   #gt.graph_draw(self.g, pos=pos, edge_color = self.edge_color,edge_pen_width = self.edge_weight,output=self.output)
    gt.graph_draw(self.g,pos=pos, vertex_size=1,edge_color = self.edge_color,output=self.output)
    logging.info('The graph %s is saved'% (self.output))
    '''
  def tree_checking(self):
    edge = self.root
    assert edge.parent == None
    while edge.children:
      #print(self.node_list)
      #print(edge.children)
      #print list(set(edge.children[0][0]).intersection(set(self.node_list)))
      #print list(set(edge.children[1][0]).intersection(set(self.node_list))) 
      assert set(edge.children[0][0]).intersection(set(self.node_list)) == set(edge.children[0][0])
      assert set(edge.children[1][0]).intersection(set(self.node_list))  == set(edge.children[1][0])
      assert edge.children[0][1]!= None and edge.children[1][1] != None
      if list(set(edge.children[0][0]).intersection(set(edge.children[1][0]))) != []:
        print("Tree erorr")
        print edge.children[0][1].parent
        print edge.children
      assert list(set(edge.children[0][0]).intersection(set(edge.children[1][0]))) == []
      edge = edge.children[0][1]
    assert edge.type == 'node'