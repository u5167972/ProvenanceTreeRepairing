# =============================================================================

# AUSTRALIAN NATIONAL UNIVERSITY OPEN SOURCE LICENSE (ANUOS LICENSE)

# VERSION 1.3

# 

# The contents of this file are subject to the ANUOS License Version 1.2

# (the "License"); you may not use this file except in compliance with

# the License. You may obtain a copy of the License at:

# 

#   http://datamining.anu.edu.au/linkage.html

# 

# Software distributed under the License is distributed on an "AS IS"

# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See

# the License for the specific language governing rights and limitations

# under the License.

# 

# The Original Software is: "ncvoter_indexing.py"

# 

# The Initial Developers of the Original Software are:

#   Peter Christen

# 

# Copyright (C) 2002 - 2011 the Australian National University and

# others. All Rights Reserved.

# 

# Contributors:

# 

# Alternatively, the contents of this file may be used under the terms

# of the GNU General Public License Version 2 or later (the "GPL"), in

# which case the provisions of the GPL are applicable instead of those

# above. The GPL is available at the following URL: http://www.gnu.org/

# If you wish to allow use of your version of this file only under the

# terms of the GPL, and not to allow others to use your version of this

# file under the terms of the ANUOS License, indicate your decision by

# deleting the provisions above and replace them with the notice and

# other provisions required by the GPL. If you do not delete the

# provisions above, a recipient may use your version of this file under

# the terms of any one of the ANUOS License or the GPL.

# =============================================================================



# =============================================================================

# Start of Febrl project module: "ncvoter_indexing.py"

#

# Generated using "guiFebrl.py" on Sat Sep 19 12:16:14 2015

# =============================================================================



# Import necessary modules (Python standard modules first, then Febrl modules)



import sys

import logging

import provenance

import rule_based_classification

import evaluation

import rules

import traceback

sys.path.append('..')

import classification

import comparison

import dataset

import encode

import indexing

import measurements

import mymath

import output

import stringcmp

import cPickle as pickle



# -----------------------------------------------------------------------------
filename = ""
fieldlist  = []
if len(sys.argv) == 2:

  ds = sys.argv[1]

# Intialise a logger, set level to info oe warning

#

log_level = logging.INFO 

#logfile is the path of the output file

logfile = './results/'+ds+'_indexing_log.txt'

hdlr = logging.FileHandler(logfile)

my_logger = logging.getLogger()

my_logger.setLevel(log_level)

my_logger.addHandler(hdlr)



# -----------------------------------------------------------------------------

# Febrl project type: Deduplicate

# -----------------------------------------------------------------------------



# -----------------------------------------------------------------------------



# Define input data set A:

#
try:
  ncvoter = "./datasets/ncvoter-20140619.csv"
  ncvoter_field_list = [("voter_id",0),

                        ("voter_reg_num",1),

                        ("name_prefix",2),

                        ("first_name",3),

                        ("middle_name",4),

                        ("last_name",5),

                        ("name_suffix",6),

                        ("age",7),

                        ("gender",8),

                        ("race",9),

                        ("ethnic",10),

                        ("street_address",11),

                        ("city",12),

                        ("state",13),

                        ("zip_code",14),

                        ("full_phone_num",15),

                        ("birth_place",16),

                        ("register_date",17),

                        ("download_month",18)]


  ncvoter_gtid = 'voter_id'

# -----------------------------------------------------------------------------

  cora = "./datasets/cora-publication.csv"
  cora_field_list = [("pid",0),

                      ("authors",1),

                      ("title",2),

                      ("name",3),

                      ("vol",4),

                      ("date",5),

                      ("id",6)]
  cora_gtid = 'id'


  # -----------------------------------------------------------------------------




  # Define field comparison functions

  #
  if ds == 'ncvoter':
    filename  = ncvoter
    fieldlist = ncvoter_field_list
    fc_funct_1 = comparison.FieldComparatorKeyDiff(agree_weight = 1.0,

                                                   description = "Key-Diff-age-age",

                                                   disagree_weight = 0.0,

                                                   missing_weight = 0.0,

                                                   max_key_diff = 1)



    fc_funct_2 = comparison.FieldComparatorWinkler(agree_weight = 1.0,

                                                   description = "Winkler-first_name-first_name",

                                                   disagree_weight = 0.0,

                                                   missing_weight = 0.0,

                                                   threshold = 0.0,

                                                   check_sim = True,

                                                   check_init = True,

                                                   check_long = True)



    fc_funct_3 = comparison.FieldComparatorWinkler(agree_weight = 1.0,

                                                   description = "Winkler-last_name-last_name",

                                                   disagree_weight = 0.0,

                                                   missing_weight = 0.0,

                                                   threshold = 0.0,

                                                   check_sim = True,

                                                   check_init = True,

                                                   check_long = True)



    fc_funct_4 = comparison.FieldComparatorNumericAbs(agree_weight = 1.0,

                                                      description = "Num-Abs-age-age",

                                                      disagree_weight = 0.0,

                                                      missing_weight = 0.0,

                                                      max_abs_diff = 1)



    fc_funct_5 = comparison.FieldComparatorLCS(agree_weight = 1.0,

                                               description = "Long-Common-Seq-full_phone_num-full_phone_num",

                                               disagree_weight = 0.0,

                                               missing_weight = 0.0,

                                               threshold = 0.0,

                                               common_divisor = "average",

                                               min_common_len = 4)



    fc_funct_6 = comparison.FieldComparatorKeyDiff(agree_weight = 1.0,

                                                   description = "Key-Diff-zip_code-zip_code",

                                                   disagree_weight = 0.0,

                                                   missing_weight = 0.0,

                                                   max_key_diff = 3)



    field_comp_list = [(fc_funct_1, "gender", "gender"),
                       (fc_funct_2, "first_name", "first_name"),
                       (fc_funct_3, "last_name", "last_name"),
                       (fc_funct_4, "age", "age"),
                       (fc_funct_5, "full_phone_num", "full_phone_num"),
                       (fc_funct_6, "zip_code", "zip_code")]

  elif ds == 'cora':
    filename  = cora
    fieldlist = cora_field_list
    fc_funct_1 = comparison.FieldComparatorQGram(agree_weight = 1.0,

                                                 description = "Q-Gram-title-title",

                                                 disagree_weight = 0.0,

                                                 missing_weight = 0.0,

                                                 threshold = 0.0,

                                                 q = 2,

                                                 common_divisor = "average",

                                                 padded = True)

    fc_funct_2 = comparison.FieldComparatorContainsString(agree_weight = 1.0,

                                                          description = "Str-Contains-authors-authors",

                                                          disagree_weight = 0.0,

                                                          missing_weight = 0.0)



    fc_funct_3 = comparison.FieldComparatorContainsString(agree_weight = 1.0,

                                                          description = "Str-Contains-date-date",

                                                          disagree_weight = 0.0,

                                                          missing_weight = 0.0)

    fc_funct_4 = comparison.FieldComparatorQGram(agree_weight = 1.0,

                                                 description = "Q-Gram-authors-authors",

                                                 disagree_weight = 0.0,

                                                 missing_weight = 0.0,

                                                 threshold = 0.0,

                                                 q = 2,

                                                 common_divisor = "average",

                                                 padded = True)

    fc_funct_5 = comparison.FieldComparatorQGram(agree_weight = 1.0,

                                                 description = "Q-Gram-name-name",

                                                 disagree_weight = 0.0,

                                                 missing_weight = 0.0,

                                                 threshold = 0.0,

                                                 q = 2,

                                                 common_divisor = "average",

                                                 padded = True)
    fc_funct_6 = comparison.FieldComparatorExactString(agree_weight = 1.0,
                                                      description = "STre-Extra-vol-vol",
                                                      disagree_weight = 0.0,
                                                      missing_weight = 0.0)


    field_comp_list = [(fc_funct_1, "title", "title"),
                       (fc_funct_2, "authors", "authors"),
                       (fc_funct_3, "date", "date"),
                       (fc_funct_4, "authors", "authors"),
                       (fc_funct_5, "name", "name"),
                       (fc_funct_6, "vol","vol")]
  dataset = dataset.DataSetCSV(description="Data set generated by Febrl GUI",

                                  access_mode="read",

                                  strip_fields=True,

                                  miss_val=[''],

                                  rec_ident="__rec_id_a__",

                                  file_name= filename,

                                  header_line=True,

                                  delimiter=",",

                                  field_list = fieldlist)
  rec_comp = comparison.RecordComparator(dataset, dataset, field_comp_list)



  # -----------------------------------------------------------------------------



  # Define indices for "blocking"

  #
  if ds == 'ncvoter':
    index_def_1 = [["last_name", "last_name", False, False, None, [encode.soundex]],

                   ["first_name", "first_name", False, False, None, [encode.soundex]]]

    index = indexing.BlockingIndex(dataset1 = dataset,

                                dataset2 = dataset,

                                progress_report = 10,

                                rec_comparator = rec_comp,

                                index_sep_str = "",

                                skip_missing = True,

                                index_def = [index_def_1]
    )




  elif ds == 'cora':

    index_def_1 = [["authors", "authors", False, False, None, [encode.dmetaphone]]]
    index = indexing.DedupIndex(dataset1 = dataset,

                                dataset2 = dataset,

                                progress_report = 10,

                                rec_comparator = rec_comp,

                                index_sep_str = "",

                                skip_missing = True,

                                index_def = [index_def_1],

                                block_method = ("block",))
  # Build and compact index

  #

  index.build()



  index.compact()

  # Do record pair comparisons

  #

  [field_names_list, w_vec_dict] = index.run()

  # -----------------------------------------------------------------------------


  # Define the rules that will be used in rule_based classifier

  rules = rules.Rules(file_name="./rules/rules.csv")

  #If the rules are currently not exist, you can add rules:
  # (rules,rule type, rule weight, the dataset that rule used to )
  #Ncvoter rules
  if ds =='ncvoter':
    rule1_id = rules.add_rule('(0,"==", 1),(4,"==",1),(1,">=",0.8),(2,">=",0.8),(3,">=",0.7)','hard+','1','ncvoter')
    rule2_id = rules.add_rule('(1,">=",0.6),(2,">=",0.6),(3,">=",0.5)','soft+','0.8','ncvoter')
    rule3_id = rules.add_rule('(1,">=", 0.7),(4,">=",0.7),(5,">=",0.7)','soft+','0.7','ncvoter')
    rule4_id = rules.add_rule('(1,">=", 0.5),(2,">=",0.5),(5,">=",0.8)','soft+','0.8','ncvoter')
    rule5_id = rules.add_rule('(0,"<", 1),(4,"<",1),(1,"<",0.2),(2,"<=",0.2)','hard-','1','ncvoter')
    rule_list = [rule1_id,rule2_id,rule3_id,rule4_id,rule5_id]
  #Cora rules
  elif ds =='cora':
    
    rule1_id = rules.add_rule('(0,">=", 0.66),(1,">=",0.3),(2,">=",0.3),(4,">=",0.01)','hard+','1','cora-publication')
    rule2_id = rules.add_rule('(0,">=", 0.7),(1,">=",0.7)','soft+','0.74','cora-publication')
    rule3_id = rules.add_rule('(0,"<",0.4),(3,"<",0.3)','hard-','1','cora-publication')
    #rule4_id = rules.add_rule('(0,">",0.1),(5,">",0.7),(4,">",0.7)','soft+','0.85','cora-publication')
    rule4_id = rules.add_rule('(0,">",0.1),(5,">",0.7),(4,">",0.9)','soft+','0.9','cora-publication')
    rule5_id = rules.add_rule('(3,">",0.5),(4,">",0.5)','soft+','0.36','cora-publication')

    #Two rules with low coverage and high precision 
    '''
    rule1_id = rules.add_rule('(0,">=", 0.66),(1,">=",0.3),(2,">=",0.3),(4,">=",0.01)','hard+','1','cora-publication') #prec 1
    rule2_id = rules.add_rule('(0,">=", 0.2),(1,">=",0.2)','soft+','0.8','cora-publication') #prec 0.1914
    rule3_id = rules.add_rule('(0,"<",0.4),(3,"<",0.3)','hard-','1','cora-publication')
    rule4_id = rules.add_rule('(0,">",0.1),(5,">",0.7),(4,">",0.7)','soft+','0.85','cora-publication') #prec 0.97
    #rule4_id = rules.add_rule('(0,">=",0.2),(3,">",0.3)','soft+','0.7','cora-publication')  #prec 0.21
    rule5_id = rules.add_rule('(3,">",0.9),(4,">",0.9)','soft+','0.9','cora-publication') # prec 0.956
    '''
    #A list of rules
    rule_list = [rule1_id,rule2_id,rule3_id,rule4_id,rule5_id]
  # to used the rules that already in rules.csv, you could use the rule by adding the id into rule_list:
  # rule_list = [rule1_id,rule2_id,1]
  
  # Call rule_based classfier
  # Filename is the path of the output file saved by pickle, which is a dictiory that contains the matches and non-matches.
  # The structure of the dictionary is :
  # {(rec_id_1,rec_id_2):[weight_vector,rule_id,rule_type]}
  classifier = rule_based_classification.RuleBased(rule_list = rule_list,rules = rules, filename= './tmp/'+ds+'_classified.pkl')

  class_w_vec_dict = w_vec_dict 

  # Classify all weight vectors

  #

  [m_set, nm_set, pm_set] = classifier.classify(class_w_vec_dict)
  del w_vec_dict

except Exception, e:
    
  logging.error(e, exc_info=True)

# WARNING: No code for classifiers provided



# WARNING: No code for output files provided





# =============================================================================

# End of Febrl project module: "ncvoter_indexing.py"

# =============================================================================

