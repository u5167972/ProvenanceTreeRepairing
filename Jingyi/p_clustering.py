# =============================================================================

# AUSTRALIAN NATIONAL UNIVERSITY OPEN SOURCE LICENSE (ANUOS LICENSE)

# VERSION 1.3

# 

# The contents of this file are subject to the ANUOS License Version 1.2

# (the "License"); you may not use this file except in compliance with

# the License. You may obtain a copy of the License at:

# 

#   http://datamining.anu.edu.au/linkage.html

# 

# Software distributed under the License is distributed on an "AS IS"

# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See

# the License for the specific language governing rights and limitations

# under the License.

# 

# The Original Software is: "ncvoter_whole.py"

# 

# The Initial Developers of the Original Software are:

#   Peter Christen

# 

# Copyright (C) 2002 - 2011 the Australian National University and

# others. All Rights Reserved.

# 

# Contributors:

# 

# Alternatively, the contents of this file may be used under the terms

# of the GNU General Public License Version 2 or later (the "GPL"), in

# which case the provisions of the GPL are applicable instead of those

# above. The GPL is available at the following URL: http://www.gnu.org/

# If you wish to allow use of your version of this file only under the

# terms of the GPL, and not to allow others to use your version of this

# file under the terms of the ANUOS License, indicate your decision by

# deleting the provisions above and replace them with the notice and

# other provisions required by the GPL. If you do not delete the

# provisions above, a recipient may use your version of this file under

# the terms of any one of the ANUOS License or the GPL.

# =============================================================================



# =============================================================================

# Start of Febrl project module: "ncvoter_whole.py"

#

# Generated using "guiFebrl.py" on Sun Aug 23 20:09:17 2015

# =============================================================================



# Import necessary modules (Python standard modules first, then Febrl modules)




import sys

import logging

import provenance

import rule_based_classification

import evaluation

import rules

import traceback

sys.path.append('..')

import classification

import comparison

import dataset

import encode

import indexing

import measurements

import mymath

import output

import stringcmp


# -----------------------------------------------------------------------------
filename = ""
field_list  = []
if len(sys.argv) == 4:
  ds = sys.argv[1]
  num_repairs = int(sys.argv[2])
  #sort_method: 0 -> random based  1-> weight based
  sort_method = int(sys.argv[3])

# Intialise a logger, set level to info oe warning

#

log_level = logging.INFO

#logfile is the path of the logging file

logfile = './results/'+ds+'_clustering_log.txt'

hdlr = logging.FileHandler(logfile)

my_logger = logging.getLogger()

my_logger.setLevel(log_level)

my_logger.addHandler(hdlr)


# -----------------------------------------------------------------------------

# Febrl project type: Deduplicate

# -----------------------------------------------------------------------------



# -----------------------------------------------------------------------------



# Define input data set A:

#
try:
  ncvoter = "./datasets/ncvoter-20140619.csv"
  ncvoter_field_list = [("voter_id",0),

                        ("voter_reg_num",1),

                        ("name_prefix",2),

                        ("first_name",3),

                        ("middle_name",4),

                        ("last_name",5),

                        ("name_suffix",6),

                        ("age",7),

                        ("gender",8),

                        ("race",9),

                        ("ethnic",10),

                        ("street_address",11),

                        ("city",12),

                        ("state",13),

                        ("zip_code",14),

                        ("full_phone_num",15),

                        ("birth_place",16),

                        ("register_date",17),

                        ("download_month",18)]


  ncvoter_gtid = 'voter_id'

# -----------------------------------------------------------------------------

  cora = "./datasets/cora-publication.csv"
  cora_field_list = [("pid",0),

                      ("authors",1),

                      ("title",2),

                      ("name",3),

                      ("vol",4),

                      ("date",5),

                      ("id",6)]
  cora_gtid = 'id'


  # -----------------------------------------------------------------------------


  if ds == 'cora':
    filename = cora
    field_list = cora_field_list
    gtid = cora_gtid
    num_vectors = 77004
  else:
    ds = 'ncvoter'
    filename = ncvoter
    field_list = ncvoter_field_list
    gtid = ncvoter_gtid
    num_vectors = 1556184
  dataset = dataset.DataSetCSV(description="Data set generated by Febrl GUI",

                                  access_mode="read",

                                  strip_fields=True,

                                  miss_val=[''],

                                  rec_ident="__rec_id_a__",

                                  file_name=filename,

                                  header_line=True,

                                  delimiter=",",

                                  field_list = field_list)
  evaluator = evaluation.Evaluation(dataset)
  #Set up a evaluator, the parameter is the field name of ground truth id
  evaluator.build(gtid)
  # filename is the path of the output pickle file genarated by rule-based classifying
  # num_vectors: The number of weight vector generated in rule-based classifying
  # num_repairs: The number of erorrs be picked each time and repaired before evaluating the quality of ER reuslt.
  # hard_rule_file: All the pair of hard matches and non-matches will be stored in this csv
  # repaired_pair: Each erroronous matches/ non-matches repaired will be record in this csv
  provenance_graph = provenance.ProvenanceGraph(evaluator = evaluator,filename = './tmp/'+ds+'_classified.pkl',num_vectors = num_vectors,num_repairs= num_repairs,
                                                hard_matches_file = './rules/'+ds+'_hard_edge.csv',repaired_pair = './rules/'+ds+'_repair.csv',sort_method = sort_method)
  provenance_graph.build_clusters()
  provenance_graph.clustering()
  #Outputs of clustering stored in memroy:
  #  provenance_graph.cluster_dict: A dictionary of cluster: key -> record_id value-> a Cluster object defined in Cluster.py
  #  provenance_graph.wrong_list: A list of erronous matches and non-matches that need to be repair

  provenance_graph.repair_result()
   #Outputs of repairing:
   #  provenance_graph.cluster_dict(repaired): A dictionary of cluster: key -> record_id value-> a Cluster object defined in Cluster.py 
   #  A file that record each user feedbacks: rules/{dataset}_repair.csv 
except Exception, e:
    
  logging.error(e, exc_info=True)
# -----------------------------------------------------------------------------



# =============================================================================

# End of Febrl project module: "ncvoter_whole.py"

# =============================================================================

