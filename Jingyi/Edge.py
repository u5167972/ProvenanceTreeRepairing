﻿class Edge:

# =============================================================================
  """ A struct of Edge
    Init_type: the edge type in G
    weight: the weight of edge
    vec_sum: the sum of vectors
    rule_id:
    current_time:
    parent:  (edge,rec tuple of the edge)
    childs: [(nodes set,edge),(node sets, edge)]
    type: the current type of edge
  """
  # ---------------------------------------------------------------------------

  def __init__(self,weight,Init_type,vec_sum,rule_id,current_time):
    self.weight = weight
    self.Init_type = Init_type
    self.vec_sum = vec_sum
    self.rule_id = rule_id
    self.current_time = current_time
    self.type = Init_type
    self.parent = None
    self.children =[]
    self.index = -1
  def change_type(self,new_type):
    self.type = new_type

  def parent(self,parent):
    self.parent = parent  
  def set_child(self,child):
    #if child not in self.children and (child[1],child[0]) not in self.children:
    self.children.append(child)
  def edge_index(self,index):
    self.index = index