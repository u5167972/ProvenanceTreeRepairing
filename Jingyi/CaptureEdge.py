# =============================================================================
# Import necessary modules
import csv
import logging
import math
import os
import random
import shelve
import string
import sys
import time
from collections import deque

sys.path.append('..')

import auxiliary


# =============================================================================
class CaptureEdge:
  """ The class to read nodes file and capture nodes
  """
  # ---------------------------------------------------------------------------

  def __init__(self, **kwargs):
    """Constructor, set general attributes.
    """
    # General attributes
    #
    self.file_name =   None   # The name of the CSV file
    self.repair_file_name = None
    self.repair_file =      None
    self.file =             None   # File pointer to current file
    self.write_quote_char = ''     # The quote character for writing fields
    self.delimiter  =       ','    # The delimiter character
    self.exists =           False
    self.rid =              0
    for (keyword, value) in kwargs.items():

      if (keyword.startswith('file')):
        auxiliary.check_is_string('file_name', value)
        self.file_name = value

      elif (keyword.startswith('repair')):
        auxiliary.check_is_string('repair_file_name', value)
        self.repair_file_name = value        
      elif (keyword.startswith('write_qu')):
        auxiliary.check_is_string('write_quote_char', value)
        self.write_quote_char = value
      elif (keyword.startswith('delimi')):
        auxiliary.check_is_string('delimiter', value)
        if (len(value) != 1):
          logging.exception('Value of "delimiter" argument must be a one-' + \
                            'character string, but it is: "%s"' % (delimiter))
          raise Exception
        self.delimiter = value

  # ---------------------------------------------------------------------------

  def finalise(self):
    """Finalise a data set, i.e. close the file and set various attributes to
       None.
    """

    # Close file if it is open
    #
    if (self.file != None):

      self.file.close()
      self.file = None
    if (self.repair_file != None):

      self.repair_file.close()
      self.repair_file = None
    self.access_mode =  None
    self.file_name =    None
    self.num_records =  None
    self.next_rec_num = None

    # A log message - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    #
    logging.info('Finalised CSV data set "%s"' % (self.description))
  # ---------------------------------------------------------------------------
  def add_rep_edge(self,rec_tuple,Init_type,rule_id,repair_wright,time,feedback):
    if (self.repair_file != None):
      self.repair_file.close()  # Close currently open file
    if(os.path.exists(self.repair_file_name)):
      try:
        self.repair_file = open(self.repair_file_name,'rb+')
        self.exists = True
      except:
        logging.exception('Cannot open CSV file "%s" for appending' % \
                            (self.repair_file_name))
    else:
      try:
        self.repair_file = open(self.repair_file_name,'wb+')
        self.exists = False
      except:
        logging.exception('Cannot open CSV file "%s" for writing' % \
                          (self.repair_file_name))
    self.csv_parser = csv.writer(self.repair_file, delimiter = self.delimiter)
    try:
      last_line = deque(csv.reader(self.repair_file, delimiter = self.delimiter), 1)[0]
      self.rid = int(last_line[0]) + 1
    except:
      self.rid = 1
    edge_tw = [rec_tuple,Init_type,rule_id,repair_wright,time,feedback]
    if (self.write_quote_char != ''):
      edge_tw = map(lambda s:self.write_quote_char+s+self.write_quote_char, edge_tw)
    self.csv_parser.writerow(edge_tw)  # Write record to file
    self.repair_file.flush()
    self.repair_file.close()

  # ---------------------------------------------------------------------------

  def add_hard_edge(self,rec_tuple,r_type,rule_id,weight,vec_sum,time):
    """Write the edge into table
    """
    if (self.file != None):
      self.file.close()  # Close currently open file
    if(os.path.exists(self.file_name)):
      try:
      	self.file = open(self.file_name,'rb+')
        self.exists = True
      except:
        logging.exception('Cannot open CSV file "%s" for appending' % \
                            (self.file_name))
    else:
      try:
        self.file = open(self.file_name,'wb+')
        self.exists = False
      except:
        logging.exception('Cannot open CSV file "%s" for writing' % \
                          (self.file_name))
    self.csv_parser = csv.writer(self.file, delimiter = self.delimiter)
    try:
      last_line = deque(csv.reader(self.file, delimiter = self.delimiter), 1)[0]
      self.rid = int(last_line[0]) + 1
    except:
      self.rid = 1
    edge_tw = [self.rid,rec_tuple,r_type,rule_id,weight,vec_sum,time]
    if (self.write_quote_char != ''):
      edge_tw = map(lambda s:self.write_quote_char+s+self.write_quote_char, edge_tw)
    self.csv_parser.writerow(edge_tw)  # Write record to file
    self.file.flush()
    self.file.close()
    return self.rid
  def get_rule(self,ids):
    """ get the rule from csv file
    """
    if (self.file != None):
      self.file.close()  # Close currently open file
    try:
      self.file = open(self.file_name,'rb')
      self.csv_parser = csv.reader(self.file, delimiter = self.delimiter)
    except:
      logging.exception('Cannot open CSV file "%s" for reading' % \
                          (self.file_name))    

    self.csv_parser = csv.reader(self.file, delimiter = self.delimiter)
    rules_list = []
    for rule in self.csv_parser:
      rule = map(string.strip, rule)
      if int(rule[0]) in ids:
        rules_list.append(rule)
    self.file.close()
    return rules_list
# =============================================================================