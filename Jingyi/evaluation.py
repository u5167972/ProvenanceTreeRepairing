import heapq
import logging
import math
import os
import random
import sys
import time
import Cluster
sys.path.append('..')

import auxiliary
import mymath


# =============================================================================

class Evaluation:
  """Evaluate the result
  """
  # ---------------------------------------------------------------------------
  def __init__(self,dataset):
    self.dataset = dataset
    self.clusters = {}
    self.result_prec = []
    self.result_reca = []
    self.result_fmeas = []
    self.result_accr = []
    self.cc_prec = []
    self.cc_reca = []
    self.cc_fmeas = []
  # ---------------------------------------------------------------------------

  def __log_build_progress__(self, count, start_time):
    """Create a log message for the number of rec done so far, the time
       used, and an estimation of much longer it will take.
    """

    used_time = time.time() - start_time
    perc_done = int(round(100.0 * count / self.dataset.num_records))
    rec_time  = used_time / count  # Time per record pair comparison
    togo_time = (self.dataset.num_records - count) * rec_time

    used_sec_str = auxiliary.time_string(used_time)
    rec_sec_str =  auxiliary.time_string(rec_time)
    togo_sec_str = auxiliary.time_string(togo_time)

    log_str = 'Parsed %d of %d records (%d%%) in %s (%s per ' % \
              (count, self.dataset.num_records, perc_done, used_sec_str,
              rec_sec_str)+'record), estimated %s until finished.' % \
              (togo_sec_str)
    logging.info(log_str)

    memory_usage_str = auxiliary.get_memory_usage()
    if (memory_usage_str != None):
      logging.info('    '+memory_usage_str)
  # ---------------------------------------------------------------------------
  def build(self,field):

    logging.info('')
    logging.info('Built 2 evaluation dictionaries in evaluation.py:  ')
    logging.info('      class_dict: key -> rec_id, value -> ground_truth_cluster_id')
    logging.info('      clusters:   key -> ground_truth_cluster_id, value -> list of rec_id')

    class_dict = {}
    done = 0  # Counter for the number of records done so far     
    start_time = time.time()
    progress_report_cnt = max(1, int(self.dataset.num_records / \
                         (100.0 / 10)))

    # Copy all records into the memory based dictionary - - - - - - - - - - - -
    field_names = []
    for (field_name, field_data) in self.dataset.field_list:
      field_names.append(field_name)
    class_index = field_names.index(field)
    for (rec_ident, rec_list) in self.dataset.readall():

      class_dict[rec_ident] = rec_list[class_index]
      l = self.clusters.get(rec_list[class_index],None)
      if not l:
       self.clusters[rec_list[class_index]] = set()
       self.clusters[rec_list[class_index]].add(rec_ident) 
      else:
        l.add(rec_ident)
      '''
      if rec_list[class_index] in self.clusters.keys():
        self.clusters[rec_list[class_index]].append(rec_ident)
      else:
        self.clusters[rec_list[class_index]] = [rec_ident]
      '''
      done += 1
      if ((done % progress_report_cnt) == 0):
        self.__log_build_progress__(done, start_time) 
      continue

    # assert self.small_dataset.num_records == len(small_data_set_dict)
    #print(class_dict)
    self.class_dict = class_dict


  # ---------------------------------------------------------------------------

  def evalute(self,rec_tuple):
    """check whether two rec are in same class
    """
    # use dict 
    if(self.class_dict[rec_tuple[0]] == self.class_dict[rec_tuple[1]]):
      return True
    else:
  	  return False
  def get_closest_cluster_measure(self,Rclusters,final):
    max_R = {}
    max_S = {}
    wrong_list = {}
    size_dict = {}
    merge_set = set()
    tp = 0
    fn = 0
    fp = 0
    tn = 0
    for rs in Rclusters:
      compared = set()
      size = rs.get_size()
      if size in size_dict:
        size_dict[size] += 1
      else:
        size_dict[size] = 1    
      for n in rs.node_list:
        node_set = set(rs.node_list)
        key = self.class_dict[n]
        if not key in compared:

          sc = self.clusters[key]
          sim = Jarcarrd_Similarity(node_set,sc) 
          insertion = node_set&sc
          tp += len(insertion)*(len(insertion)-1)/2 
          record  = 0 
          for n in (node_set - insertion):
            for n_1 in insertion:
              fp += 1 
              wrong_list[(n,n_1)] = 'split'
          for n in sc - insertion:
            for n_1 in insertion:

              if (n,n_1) not in merge_set:
                record +=1 
                fn += 1

                merge_set.add((n,n_1))
                wrong_list[(n,n_1)] = 'merge'    
          r_value = max_R.get(rs,0)
          #if rs not in max_R.keys():
          #  max_R[rs] = sim
          if r_value < sim:
            max_R[rs] = sim
          s_value = max_S.get(key,0)
          #if key not in max_S.keys():
          #  max_S[key] = sim
          if s_value < sim:
            max_S[key] = sim    
        compared.add(key)

      '''
      for key,sc in self.clusters.items():
        sim = Jarcarrd_Similarity(set(rs.node_list),set(sc)) 
        insertion = set(rs.node_list)&set(sc)
        tp += len(insertion)*(len(insertion)-1)/2 
        record  = 0 
        for n in (set(rs.node_list) - insertion):
          for n_1 in insertion:
            fp += 1 
            wrong_list[(n,n_1)] = 'split'
        for n in set(sc) - insertion:
          for n_1 in insertion:
            record +=1 
            if (n,n_1) not in merge_set:
              fn += 1

              merge_set.add((n,n_1))
              wrong_list[(n,n_1)] = 'merge'
        tn += (self.dataset.num_records - len(rs.node_list) - record) * len(insertion)
        r_value = max_R.get(rs,0)
        #if rs not in max_R.keys():
        #  max_R[rs] = sim
        if r_value < sim:
          max_R[rs] = sim
        s_value = max_S.get(key,0)
        #if key not in max_S.keys():
        #  max_S[key] = sim
        if s_value < sim:
          max_S[key] = sim
        '''
    fn = fn/2
    tn = (self.dataset.num_records * self.dataset.num_records) - tp - fp - fn
    prec = float(sum(max_R.values()) / len(max_R))
    reca = float(sum(max_S.values())/len(max_S))
    fmeas = 2*(prec*reca) / (prec+reca)
    self.cc_fmeas.append(fmeas)
    self.cc_reca.append(reca)
    self.cc_prec.append(prec)
    result_list = [tp,fn,fp,tn]
    self.get_measures(result_list)
    logging.info('  Closest cluster results: precision = %f, recall = %f, f-measure = %f; ' % \
                (prec,reca,fmeas))   
    logging.info('')
    return wrong_list,size_dict,fn,fp
  def get_measures(self,result_list):
    """Function which calculates quality measures from raw classification
       counts.
       Returns accuracy, precision, recall, and f-measure values.
    """

    tp, fn, fp, tn = result_list
    tp = float(tp)
    tn = float(tn)
    fp = float(fp)
    fn = float(fn)
    logging.info(' TP %d, FN %d, FP %d, TN %d'%(tp,fn,fp,tn))
    if ((tp != 0) or (fp != 0) or (tn != 0) or (fn != 0)):
      acc = (tp + tn) / (tp + fp + tn + fn)
    else:
      acc = 0.0

    if ((tp != 0) or (fp != 0)):
      prec = tp / (tp + fp)
    else:
      prec = 0.0

    if ((tp != 0) or (fn != 0)):
      reca = tp / (tp + fn)
    else:
      reca = 0.0
    if ((prec != 0.0) or (reca != 0.0)):
      fmeas = 2*(prec*reca) / (prec+reca)
    else:
      fmeas = 0.0
    logging.info('  Results: accuracy = %f, precision = %f, recall = %f, f-measure = %f; ' % \
                (acc,prec,reca,fmeas))
    self.result_prec.append(prec)
    self.result_reca.append(reca)
    self.result_fmeas.append(fmeas)
    self.result_accr.append(acc)

    return acc, prec, reca, fmeas
  def clean_result_list(self):
    self.result_prec = []
    self.result_reca = []
    self.result_fmeas = []
    self.result_accr = []
    self.cc_prec = []
    self.cc_reca = []
    self.cc_fmeas = []
  def print_measures_list(self):
    logging.info('precision')
    logging.info(self.result_prec)
    logging.info('recall')
    logging.info(self.result_reca)
    logging.info('accuracy')
    logging.info(self.result_accr)
    logging.info('f-measure')
    logging.info(self.result_fmeas)

    #############

    logging.info('Closest cluster precision')
    logging.info(self.cc_prec)
    logging.info('recall')
    logging.info(self.cc_reca)
    logging.info('f-measure')
    logging.info(self.cc_fmeas)

def Jarcarrd_Similarity(r,s):

  return abs(float(len(r&s))/float(len(r|s)))

