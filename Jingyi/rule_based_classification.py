#A basic implementation of rule-based classification
# =============================================================================
# Import necessary modules (Python standard modules first, then Febrl modules)
import heapq
import logging
import math
import os
import random
import sys
import evaluation 
import rules
import time
from ast import literal_eval as make_tuple
import cPickle as pickle
sys.path.append('..')

import auxiliary
import mymath
import classification

try:
  import svm
  imp_svm = True
except:
  imp_svm = False

#if (imp_pyml == False):
#  logging.warn('Cannot import Numeric and PyML modules')
if (imp_svm == False):
  logging.warn('Cannot import svm module')

  # =============================================================================

class RuleBased(classification.Classifier):
  """Implements the classical rule-based classifier.

     This classifier sums all weights in a weight vector into one matching
     weight and then uses two threshold to classify this weight vector as
     either a match, non-match or a possible match.

     The arguments that have to be set when this classifier is initialised is:

       rule_list A list that contains the predicate tuples: [index,<>=, threshold]
  """

  # ---------------------------------------------------------------------------

  def __init__(self, **kwargs):
    """Constructor. Process the 'rule_list' argument first, then call the base
       class constructor.
    """
    self.prov_graph = None
    self.rule_list = None
    self.rule_id_list = None
    base_kwargs = {}  # Dictionary, will contain unprocessed arguments for base
                      # class constructor
    self.edges = {}
    self.file = None
    self.filename = None
    for (keyword, value) in kwargs.items():

      if (keyword.startswith('rule_list')):
        auxiliary.check_is_list('rule_list',value)
        self.rule_id_list = value
     # elif(keyword.startswith('evaluator')):
     #   self.evaluator = value
      elif(keyword.startswith('rules')):
      	self.rules = value
      elif(keyword.startswith('filename')):
        self.filename = value
      else:
        base_kwargs[keyword] = value

    classification.Classifier.__init__(self, base_kwargs)  # Initialise base class

    # Check threshold values are set and valid - - - - - - - - - - - - - - - -
    #
    auxiliary.check_is_list('rule_list', self.rule_id_list)

    # If the weight vector dictionary and both match and non-match sets - - - -
    # are given start the training process
    #
    if ((self.train_w_vec_dict != None) and (self.train_match_set != None) \
        and (self.train_non_match_set != None)):
      logging.info('Train Rule-Based classifier: "%s"' % \
                   (self.description))
      logging.info('  Nothing needs to be done.')
  # ---------------------------------------------------------------------------

  def __log_clustering_progress__(self, count, start_time,size):
    """Create a log message for the number of clustering done so far, the time
       used, and an estimation of much longer it will take.
    """

    used_time = time.time() - start_time
    perc_done = int(round(100.0 * count / size))
    rec_time  = used_time / count  # Time per record pair comparison
    togo_time = (size - count) * rec_time

    used_sec_str = auxiliary.time_string(used_time)
    rec_sec_str =  auxiliary.time_string(rec_time)
    togo_sec_str = auxiliary.time_string(togo_time)

    log_str = 'Parsed %d of %d Classifier (%d%%) in %s (%s per ' % \
              (count, size, perc_done, used_sec_str,
              rec_sec_str)+'pair), estimated %s until finished.' % \
              (togo_sec_str)
    logging.info(log_str)

    memory_usage_str = auxiliary.get_memory_usage()
    if (memory_usage_str != None):
      logging.info('    '+memory_usage_str)
  # ---------------------------------------------------------------------------

  def train(self, w_vec_dict, match_set, non_match_set):
    """Method to train a classifier using the given weight vector dictionary
       and match and non-match sets of record identifier pairs.

       Nothing needs to be done.
    """

    logging.info('')
    logging.info('Train Rule-Based classifier: "%s"' % \
                 (self.description))
    logging.info('  Nothing needs to be done.')

  # ---------------------------------------------------------------------------

  def test(self, w_vec_dict, match_set, non_match_set,poss_match_set):
    """Method to test a classifier using the given weight vector dictionary and
       match and non-match sets of record identifier pairs.

       Will return a confusion matrix as a list of the form: [TP, FN, FP, TN].

       The numbers returned in the confusion matrix will only consider the
       classified matches and non-matches, but not the possible matches.

       TODO:
       - Is this correct, does this makes sense?
    """

    auxiliary.check_is_dictionary('w_vec_dict', w_vec_dict)
    auxiliary.check_is_set('match_set', match_set)
    auxiliary.check_is_set('non_match_set', non_match_set)

    # Check that match and non-match sets are separate and do cover all weight
    # vectors given
    #
    if (len(match_set.intersection(non_match_set)) > 0):
      logging.exception('Intersection of match and non-match set not empty')
      raise Exception


    logging.info('')
    logging.info('Testing Rule-Based classifier using %d weight ' % \
                 (len(w_vec_dict))+'vectors')
    logging.info('  Match and non-match sets with %d and %d entries' % \
                 (len(match_set), len(non_match_set)))

    num_true_m =   0
    num_false_m =  0
    num_true_nm =  0
    num_false_nm = 0
    num_poss_m =   0
    for (rec_id_tuple) in match_set:
      if(self.evaluator.evalute(rec_id_tuple)):
        num_true_m += 1
      else:
        self.log([('FP', rec_id_tuple)])
        self.log([('vector', w_vec_dict[rec_id_tuple])])

        num_false_m += 1
    for (rec_id_tuple) in non_match_set:
      if(self.evaluator.evalute(rec_id_tuple)):
        self.log([('FN', rec_id_tuple)])
        self.log([('vector', w_vec_dict[rec_id_tuple])])
        num_false_nm += 1
      else:
        num_true_nm +=1
    for (rec_id_tuple) in poss_match_set:
      if(self.evaluator.evalute(rec_id_tuple)):
        num_poss_m += 1      
    #assert (num_true_m+num_false_nm+num_false_m+num_true_nm+num_poss_m) == \
    #       len(w_vec_dict)

    logging.info('  Results: TP = %d, FN = %d, FP = %d, TN = %d; ' % \
                 (num_true_m,num_false_nm,num_false_m,num_true_nm) + \
                 'possible matches = %d total possmatch =%d' % (num_poss_m,len(poss_match_set)))

    return [num_true_m, num_false_nm, num_false_m, num_true_nm]

  # ---------------------------------------------------------------------------

  def cross_validate(self, w_vec_dict, match_set, non_match_set,poss_match_set, n=10):
    """Method to conduct a cross validation using the given weight vector
       dictionary and match and non-match sets of record identifier pairs.

       Will return a confusion matrix as a list of the form: [TP, FN, FP, TN].

       See documentation of 'test' for more information.
    """

    logging.info('')
    logging.info('Cross validation for Rule-Based is the same as' + \
                 'testing.')

    return self.test(w_vec_dict, match_set, non_match_set,poss_match_set)

  # ---------------------------------------------------------------------------

  def classify(self, w_vec_dict):
    """Method to classify the given weight vector dictionary using the trained
       classifier.

       Will return three sets with record identifier pairs: 1) match set,
       2) non-match set, and 3) possible match set
    """
    progress_report_cnt = max(1, int(len(w_vec_dict) / \
                         (100.0 / 10)))
    auxiliary.check_is_dictionary('w_vec_dict', w_vec_dict)
    f = open(self.filename, 'w')
    start_time = time.time() 
    logging.info('')
    logging.info('Classify %d weight vectors using Rule- ' % \
                 (len(w_vec_dict))+'classifier')

    match_set =      set()
    non_match_set =  set()
    poss_match_set = set()
    comp_done = 0
    for (rec_id_tuple, w_vec) in w_vec_dict.iteritems():
      #print("test 1")

      result = self.rule_handle(w_vec)
      #If there is an edge between two record
      if result:
        if 'hard' in result[2]:
          if(result[0]):
        	  match_set.add(rec_id_tuple)
      	  else:
        	  non_match_set.add(rec_id_tuple)
        elif(result[2] == 'soft+'):
          poss_match_set.add(rec_id_tuple)
        out_result = {'w_vec': w_vec,
                      'rec_id_tuple':rec_id_tuple,
                      'rule_id':result[1],
                      'rule_type':result[2],
                      'rule_weight':result[3]}

        str_output = str(rec_id_tuple) + ";" + str(result[2]) +"\n"
        #print(str_output)
        #self.prov_graph.connect(w_vec,rec_id_tuple,result[1],result[2],result[3])
      
      else:
        out_result = {'rec_id_tuple':rec_id_tuple}
        #self.prov_graph.build_single_cluster(rec_id_tuple)
      del result
      pickle.dump(out_result, f)
      #self.edges[rec_id_tuple] = result[2]
      comp_done += 1
      if ((comp_done % progress_report_cnt) == 0):
        self.__log_clustering_progress__(comp_done, start_time,len(w_vec_dict))   
    #self.clustring(w_vec_dict)
    f.close()
    num_vectors  = len(w_vec_dict)
    #result_list = self.cross_validate(w_vec_dict,match_set,non_match_set,poss_match_set)
    del w_vec_dict

    logging.info('Classified %d weight vectors: %d as matches, %d as ' % \
                 (num_vectors, len(match_set), len(non_match_set)) + \
                 'non-matches')
    #self.evaluator.get_measures(result_list)
    #clusters = self.prov_graph.clustering()
    end_time = time.time() 
    logging.info('Total takes %s for matching '% \
                auxiliary.time_string(end_time - start_time))

    return match_set, non_match_set, poss_match_set

  # ---------------------------------------------------------------------------
  def rule_handle(self,w_vec):
    """Method to handle the rules.
       Will return the boolean result
    """

    auxiliary.check_is_list('rule_id_list',self.rule_id_list)
    auxiliary.check_is_list('w_vec',w_vec)
    soft_list = []
    self.rule_list = self.rules.get_rule(self.rule_id_list)
    for rule_info in self.rule_list:
      rule_id = rule_info[0]
      rule_content = eval(rule_info[1])
      rule_type = rule_info[2]
      rule_weight = rule_info[3]
      num_true = 0
      for rule_tuple in rule_content:
      	#get agruments from rule tuple
      	flag = False
      	num_restrain = len(rule_content)
      	# When there is only one restrain
      	if(type(rule_tuple) is int):
      		rule_tuple = rule_content
      		num_restrain = 1
      		flag = True
        index = rule_tuple[0]
        auxiliary.check_is_integer('index',index)

        symbol = rule_tuple [1]
        auxiliary.check_is_string('symbol',symbol)
        threshold = rule_tuple[2]
        auxiliary.check_is_positive('threshold',threshold)
        if(symbol == "=="):
          if(w_vec[index] == threshold):
            num_true += 1
        elif(symbol == ">"):
          if(w_vec[index] > threshold):
            num_true += 1
        elif(symbol == "<"):
          if(w_vec[index] < threshold):
            num_true += 1
        elif(symbol == ">="):
          if(w_vec[index] >= threshold):
            num_true += 1
        elif(symbol == "<="):
          if(w_vec[index] <= threshold):
            num_true += 1
        if flag == True:
        	break
      if(num_true == num_restrain):
        if 'hard+' == rule_type:
          return True,rule_id,rule_type,rule_weight
        elif 'hard-' == rule_type:
          return False,rule_id,rule_type,rule_weight
        elif 'soft+' == rule_type:
          soft_list.append((True,rule_id,rule_type,rule_weight))
        else:
          soft_list.append((False,rule_id,rule_type,rule_weight))
    pos = 0
    neg = 0
    pos_weight = 0.0
    neg_weight = 0.0
    pos_rule_id = 0
    neg_rule_id = 0
    soft_weight = 0
    for soft_rule in soft_list:
      if soft_rule[2] == 'soft+':
        pos += 1
        pos_weight += float(soft_rule[3])
        pos_rule_id = soft_rule[1]
      else:
        neg +=1
        neg_weight = neg_weight + float(soft_rule[3])
        neg_rule_id = soft_rule[1]
    if(pos>0 and neg>0):
      soft_weight = pos_weigth/pos - neg_weight/neg
    elif(pos>0):
      soft_weight = pos_weight
    elif(neg>0):
      soft_weight = neg_weight
    if soft_weight > 0:
      return True,pos_rule_id,'soft+',pos_weight/pos
    elif soft_weight < 0:
      return False,neg_rule_id,'soft-',neg_weight/neg
    else:
      return None

# =============================================================================
