import math
import os
import random
import datetime
import sys
#sys.setrecursionlimit(10000)
import frontiers
import logging
import Edge
import Cluster
import time
import evaluation 
import CaptureEdge
from collections import deque
#import graph_tool.all as gt
import cPickle as pickle
import copy

sys.path.append('..')
import auxiliary
import mymath

# =============================================================================

class ProvenanceGraph:

  """Build a weighted graph to store the provenance inforamtion.
  	 The grah is implemented by using dictiory
  	 The structure will be :
  	 {record_1:
  	 		{record2:sum_of_weight,[(comparision vector, weight),(comparision vector, weight)],
  	 		 record3:....
  	 		}
  	 }

  """
  # ---------------------------------------------------------------------------
  def __init__(self, **kwargs):
    """initialization a graph.
    """
    self.cluster_dict= {}
    self.num_weight_vectors = 0
    self.records = set()
    self.repair_list = []
    self.edges = {}
    self.wrong_tr = 0 
    self.true_tr = 0
    self.wrong_tr_list = []
    self.wrong_fn_list = []
    self.wrong_list = {}
    self.num_tp = 0
    self.num_fp = 0
    self.num_fn = 0
    self.num_tn = 0
    self.filename = None
    self.mnodes= {}
    self.sort_method = 0
    self.hard_neg_edges = {}
    self.repair_method = 0
    for (keyword, value) in kwargs.items():

      if(keyword.startswith('evaluator')):
        self.evaluator = value
      elif(keyword.startswith('filename')):
        self.filename = value
      elif(keyword.startswith('sort')):
        self.sort_method = value
      elif(keyword.startswith('num_vectors')):
        self.num_weight_vectors = value
      elif(keyword.startswith('num_repairs')):
        self.num_repairs = value
      elif(keyword.startswith('hard')):
        self.hard_edge_file = value
      elif(keyword.startswith('repaired_pair')):
        self.repair_file = value
      elif(keyword.startswith('repair_method')):
        self.repair_method = value
    self.captureEdge = CaptureEdge.CaptureEdge(file_name=self.hard_edge_file,repair_file_name=self.repair_file)

  # ---------------------------------------------------------------------------

  def __log_clustering_progress__(self, count, start_time):
    """Create a log message for the number of clustering done so far, the time
       used, and an estimation of much longer it will take.
    """

    used_time = time.time() - start_time
    clusters_size = len(set(self.cluster_dict.values()))
    perc_done = int(round(100.0 * count /clusters_size))
    rec_time  = used_time / count  # Time per record pair comparison
    togo_time = (clusters_size- count) * rec_time

    used_sec_str = auxiliary.time_string(used_time)
    rec_sec_str =  auxiliary.time_string(rec_time)
    togo_sec_str = auxiliary.time_string(togo_time)

    log_str = 'Parsed %d of %d clusters (%d%%) in %s (%s per ' % \
              (count, clusters_size, perc_done, used_sec_str,
              rec_sec_str)+'pair), estimated %s until finished.' % \
              (togo_sec_str)
    logging.info(log_str)

    memory_usage_str = auxiliary.get_memory_usage()
    if (memory_usage_str != None):
      logging.info('    '+memory_usage_str)
  def __log_reading_progress__(self, count, start_time):

    used_time = time.time() - start_time
    perc_done = int(round(100.0 * count / self.num_weight_vectors))
    rec_time  = used_time / count  # Time per record pair comparison
    togo_time = (self.num_weight_vectors- count) * rec_time

    used_sec_str = auxiliary.time_string(used_time)
    rec_sec_str =  auxiliary.time_string(rec_time)
    togo_sec_str = auxiliary.time_string(togo_time)

    log_str = 'Parsed %d of %d weight vectors (%d%%) in %s (%s per ' % \
              (count,self.num_weight_vectors, perc_done, used_sec_str,
              rec_sec_str)+'pair), estimated %s until finished.' % \
              (togo_sec_str)
    logging.info(log_str)

    memory_usage_str = auxiliary.get_memory_usage()
    if (memory_usage_str != None):
      logging.info('    '+memory_usage_str)

  # ---------------------------------------------------------------------------
  def build_clusters(self):
    """Base on the input file to build clusters, which each cluster contain the
       nodes that linked by either + or = edges.
    """
    logging.info('')
    logging.info('Start building initial clusters by only considering soft and hard matches in provenance.py build_clusters')  
    start_time = time.time()
    progress_report_cnt = max(1, int(self.num_weight_vectors / \
                         (100.0 / 10)))


    f = open(self.filename, 'rb')
    count = 0
    while True:
      try:

          o = pickle.load(f)
          if('w_vec' in o.keys()):
            self.connect(o['w_vec'],o['rec_id_tuple'],o['rule_id'],o['rule_type'],o['rule_weight'])
          else:
            self.build_single_cluster(o['rec_id_tuple'])
      except EOFError:
          logging.info("")
          break
      #print('count',count)

      count += 1
      if ((count % progress_report_cnt) == 0):
        self.__log_reading_progress__(count, start_time) 
      continue
    f.close()
    for record in (set(self.evaluator.class_dict.keys())-self.records):
      cluster = Cluster.Cluster(record)
      cluster.root = Edge.Edge(0,'node',0,-1,None)
      #self.clusters.append(cluster)
      self.records.add(record)
      self.cluster_dict[record] = cluster

  # ---------------------------------------------------------------------------
  def connect(self,w_vec,rec_tuple,rule_id,rule_type,rule_weight):
    """ Add link between two records, and store 
  		the provenance information
  		Arguments:
  		rec_tuple: two records that will be linked
      rule_id: ID of the rule used in matching
  		  			
    """

    #print('connect')

    for rec in rec_tuple:
      self.records.add(rec)
    node1 = rec_tuple[0]
    node2 = rec_tuple[1]

    if rec_tuple[0] in self.mnodes:
      node1 = self.mnodes[rec_tuple[0]]
    if rec_tuple[1] in self.mnodes:
      node2 = self.mnodes[rec_tuple[1]]

    #store all the edges
    #self.edges.insert(rec_tuple,rule_type)
    #print(rule_type)
    if node1 != node2:
      if rule_type == 'hard+':
        self.build_group(node1,node2,rule_type)
        self.merge_node(node1,node2)
        return

      e = self.get_edge(node1,node2)
      if e and e.weight<rule_weight:
        try:
          del self.edges[(node1,node2)]
        except KeyError:
          pass
        try:
          del self.edges[(node2,node1)]
        except KeyError:
          pass
        e = None
      #print("new edge")

      if not e:
        current_time = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')

        e = Edge.Edge(rule_weight,rule_type,sum(w_vec),rule_id,current_time)
        self.edges[(node1,node2)] = e

        if 'hard' in rule_type:
          self.captureEdge.add_hard_edge(rec_tuple,rule_type,rule_id,rule_weight,sum(w_vec),current_time)
      if '+' in rule_type:
        #print('build group')

        self.build_group(node1,node2,rule_type)

    if rule_type == 'hard-':
      #cluster = self.find_cluster(node2,self.clusters)
      c  = self.cluster_dict.get(node2,None)
      if not c:
        cluster = Cluster.Cluster(node2)
        #self.clusters.append(cluster)
        self.cluster_dict[node2] = cluster
        self.add_hard_edge(rec_tuple[1],rec_tuple)      

      c  = self.cluster_dict.get(node1,None)
      if not c:
        cluster = Cluster.Cluster(node1)
        #self.clusters.append(cluster)
        self.cluster_dict[node1] = cluster
        self.add_hard_edge(rec_tuple[0],rec_tuple)



  def add_hard_edge(self,rec,rec_tuple):
    if rec not in self.hard_neg_edges:
      self.hard_neg_edges[rec] = [rec_tuple]
    else:
      if rec_tuple not in self.hard_neg_edges[rec] and (rec_tuple[1],rec_tuple[0]) not in self.hard_neg_edges[rec]:
        self.hard_neg_edges[rec].append(rec_tuple)
  # ---------------------------------------------------------------------------
  def merge_node(self,node1,node2):
    """ merge the node connect by = into one node, and replace the new node in
        both clustering graphs and provenance graphs.
    """
    #print('merge_node')
    edges_to_merge = {}
    cluster = self.find_cluster(node1)
    cluster2 = self.find_cluster(node2)

    try:
      del self.cluster_dict[node1]
    
      del self.cluster_dict[node2]
    except KeyError:
      print 'Keyerror',node2,node1

    if node1 in cluster.ng:
      cluster.ng.remove(node1)
    if node2 in cluster.ng:
      cluster.ng.remove(node2)

    new_node_name = node1 + "&" + node2



    if (node1,node2) in cluster.edge_label:
      cluster.edge_label.remove((node1,node2))
    if (node2,node1) in cluster.edge_label:
      cluster.edge_label.remove((node2,node1))
    if new_node_name not in cluster.ng:
      cluster.ng.append(new_node_name)


    self.cluster_dict[new_node_name] = cluster
    neg_edge_list = []
    if node1 in self.hard_neg_edges.keys():
      neg_list = self.hard_neg_edges[node1]
      for rec_tuple in neg_list:
        index = rec_tuple.index(node1)
        lst = list(rec_tuple)
        lst[index] = new_node_name
        key = tuple(lst)
        neg_edge_list.append(key)
    if node2 in self.hard_neg_edges.keys():
      neg_list = self.hard_neg_edges[node2]
      for rec_tuple in neg_list:
        index = rec_tuple.index(node2)
        lst = list(rec_tuple)
        lst[index] = new_node_name
        key = tuple(lst)
        neg_edge_list.append(key)
    self.hard_neg_edges[new_node_name] = neg_edge_list
    edge_label = cluster.edge_label[:]
    for key in edge_label:
      index1=index2=-1
      if node1 in key:
        index1 = key.index(node1)
      if node2 in key:
        index2 = key.index(node2)
      if index1 > -1 and index2 > -1:
        self.del_edge(key)
        cluster.remove_edge(key)
      elif index1 > -1 :

        edge = self.get_edge(key[0],key[1])

        result = cluster.remove_edge(key)

        self.del_edge(key)
        lst = list(key)
        lst[index1] = new_node_name #new node name
        key = tuple(lst)
        if result:
          cluster.insert_edge(key)
        #if key not in cluster.edge_label:
        #  cluster.edge_label.append(key)

        
        o_edge = self.edges.get(key,'notfind')
        if o_edge == 'notfind':
          o_edge = self.edges.get((key[1],key[0]),'notfind')
        if o_edge != 'notfind':
          self.del_edge(key)
          edges_to_merge[key] = [o_edge,edge]      
        else:
          self.edges[key] = edge
        
      elif index2 > -1:

        edge = self.get_edge(key[0],key[1])

        result = cluster.remove_edge(key)


        self.del_edge(key)
        lst = list(key)
        lst[index2] = new_node_name #new node name
        key = tuple(lst)
        if result:
          cluster.insert_edge(key)
        #if key not in cluster.edge_label:
        #  cluster.edge_label.append(key)

        o_edge = self.edges.get(key,'notfind')
        if o_edge == 'notfind':
          o_edge = self.edges.get((key[1],key[0]),'notfind')
        if o_edge != 'notfind':
          self.del_edge(key)
          edges_to_merge[key] = [o_edge,edge]      
        else:
          self.edges[key] = edge
        
    for key,value in edges_to_merge.items():
      #the value should be a pair of edges
      edge1 = value[0]
      edge2 = value[1]
      if edge1.type  == edge2.type:
        sum_vec = (edge1.vec_sum + edge2.vec_sum)/2
        #print(edge1.weight,edge2.weight)
        weight = (float(edge1.weight)+ float(edge2.weight)) /2
        current_time = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
        if(edge1.type == 'hard+'):
          edge = Edge.Edge(weight,'soft+',sum_vec,0,current_time)
          edge.type = 'hard+'
        elif(edge1.type == 'hard-'):
          edge = Edge.Edge(weight,'soft-',sum_vec,0,current_time)
          edge.type = 'hard-'

        else:
          edge = Edge.Edge(weight,edge1.type,sum_vec,0,current_time)

        self.edges[key] = edge
      else:
        if(edge1.type == 'hard-'):
          self.edges[key] = edge1
        elif (edge2.type == 'hard-'):
          self.edges[key] = edge2



    for n in new_node_name.split("&"):
      self.mnodes[n] = new_node_name

    return new_node_name
  # ---------------------------------------------------------------------------
  def del_edge(self,rec_tuple):

    if self.edges.get(rec_tuple,None):
      del self.edges[rec_tuple]
      return
    if self.edges.get((rec_tuple[1],rec_tuple[0]),None):
      del self.edges[(rec_tuple[1],rec_tuple[0])]
      return
  def clustering(self):
    """ Implementation the Single Graph Algorithm from 'Large-Scale Deduplication with Constraints using dedupalog'
    """
    #permutate set of nodes randomly
    unprocessed_cluster = list(set(self.cluster_dict.values()))

    size = len(unprocessed_cluster)
    logging.info('')
    strategy = ''
    if self.sort_method == 0:
      strategy = 'random'
    else:
      strategy = 'weight-based'
    logging.info('Start to do clustring based on %d initial clusters and hard non-matches and build provenance trees using %s strategy'%(size,strategy))  
    #self.count_edge_types()
    comp_done = 0  # Counter for the number of clusters done so far     
    start_time = time.time()
    progress_report_cnt = max(1, int(size / \
                         (100.0 / 10)))

    for cluster in unprocessed_cluster:
      if len(cluster.node_list) == 1 or len(cluster.edge_label) == 0 :
        cluster.root = Edge.Edge(0,'node',0,-1,None)

        comp_done += 1
        if ((comp_done % progress_report_cnt) == 0):
          self.__log_clustering_progress__(comp_done, start_time) 
        continue

      subclusters = {}
      lnode = cluster.node_list[:]
      for ng in cluster.ng:
        nodes = ng.split("&")
        n = nodes.pop()
        c = Cluster.Cluster(n)
        c.ng.append(ng)
        self.cluster_dict[ng] = c

        lnode.remove(n)
        while nodes:
          n = nodes.pop()
          c.node_list.append(n)

          lnode.remove(n)
        subclusters[c] = Edge.Edge(0,'node',0,-1,None)
        c.root = subclusters[c]
      for node in lnode:
        c = Cluster.Cluster(node)
        subclusters[c] = Edge.Edge(0,'node',0,-1,None)
        c.root = subclusters[c]
        self.cluster_dict[node] = c

      edge_index = self.sort_edge(self.sort_method,cluster)

      while edge_index:
        merged_cluster = None
        merged = False
        rec_tuple = edge_index.pop(0)
        edge = self.get_edge(rec_tuple[0],rec_tuple[1])

        if(edge.type == "soft+"):
          # link node edge
          lcluster = None
          rcluster = None
          rec1 = rec_tuple[0].split('&')
          rec2 = rec_tuple[1].split('&')

          neg_edges = []
          for subcluster in subclusters.keys():
            if set(rec1) == get_intersection(rec1,subcluster.node_list) and  list(get_intersection(rec2,subcluster.node_list)) == [] :
              lchild = subcluster.node_list
              lcluster= subcluster
            elif set(rec2) == get_intersection(rec2,subcluster.node_list) and list(get_intersection(rec1,subcluster.node_list)) == [] :
              rchild = subcluster.node_list
              rcluster = subcluster
              #print(rec_tuple)
              
              #del rcluster
          #if merged:
          #  continue
          merged_cluster = copy.deepcopy(lcluster)
          merged_cluster.insert(rec_tuple,rec_tuple[1])

          for edge_label in rcluster.edge_label:
            merged_cluster.merge(edge_label)

          edge.type = "hard+"

          if rec_tuple[0] in self.hard_neg_edges.keys():
            neg_edges = neg_edges + self.hard_neg_edges[rec_tuple[0]]
          if rec_tuple[1] in self.hard_neg_edges.keys():
            neg_edges = neg_edges + self.hard_neg_edges[rec_tuple[1]]

          subclusters[lcluster].parent = (edge,rec_tuple)
          subclusters[rcluster].parent = (edge,rec_tuple)

          edge.set_child((lchild,subclusters[lcluster]))
          edge.set_child((rchild,subclusters[rcluster]))
          del subclusters[rcluster]
          del rcluster
          del subclusters[lcluster]
          del lcluster
          edges = edge_index[:]
          for edge_tuple in edges:
            edge_list = edge_tuple[0].split("&") + edge_tuple[1].split("&")
            
            if set(edge_list) == set(get_intersection(edge_list,merged_cluster.node_list)):
              e = self.get_edge(edge_tuple[0],edge_tuple[1])
              e.type = 'hard+'
              edge_index.remove(edge_tuple)
              merged_cluster.insert_edge(edge_tuple)

          '''except KeyError e:
            print e
            pass    
          '''

          merged_cluster.root = edge
          subclusters[merged_cluster] = edge
          for subcluster in subclusters.keys():
            if subcluster != merged_cluster:
              edge_index = self.resolve_edge(merged_cluster,subcluster.node_list,neg_edges,edge_index)

          #print('cluster edge_label',lcluster.edge_label)

      for c in subclusters:
        #print(c.node_list)
        #self.clusters.append(c)
        for rec in c.node_list:
          rec = self.mnodes.get(rec,rec)
          self.cluster_dict[rec] = c
          '''
          if rec in self.mnodes.keys():
            self.cluster_dict[self.mnodes[rec]] = c
          else:
            self.cluster_dict[rec] = c
          '''


      comp_done += 1
      if ((comp_done % progress_report_cnt) == 0):
        self.__log_clustering_progress__(comp_done, start_time)  
    self.clusters = list(set(self.cluster_dict.values()))

    logging.info('Final clusters: %d', len(self.clusters))  
    self.count_edge_types()
    total_size = 0
    self.wrong_list = {}    

    index = 0

    self.wrong_list,size_dict,fn,fp = self.evaluator.get_closest_cluster_measure(self.clusters,False)
    self.initial_errors = fp+fn
    #self.evaluator.get_measures([self.num_tp, self.num_fn, self.num_fp, self.num_tn])
    logging.info('Clustering total nodes: %d,Average size of the cluster %.2f' % (len(self.records),float(len(self.records))/len(self.clusters)))
    ("records received %d" %len(self.records))
    logging.info('Clustering distribution:')
    logging.info('Size:')
    logging.info(size_dict.keys())
    logging.info("Number of clusters")
    logging.info(size_dict.values())
    
  # ---------------------------------------------------------------------------
    
  def resolve_edge(self,cluster,nodeset2,neg_edges,edge_index):
    nodeset1 = cluster.node_list
    #first_edge = False
    neg_edge = False
    edge_list = edge_index[:]

    for neg_rec in neg_edges:
      rec1 = neg_rec[0].split("&")
      rec2 = neg_rec[1].split("&")
      if set(rec1) == set(get_intersection(rec1,nodeset2)) or set(rec2) == set(get_intersection(rec2,nodeset2)):
        neg_edge = True
    for edge_tuple in edge_list:
      rec1 = set(edge_tuple[0].split("&"))
      rec2 = set(edge_tuple[1].split("&"))

      if (rec1 == rec1.intersection(set(nodeset1)) and rec2 == rec2.intersection(set(nodeset2))) or (rec2 == rec2.intersection(set(nodeset1)) and rec1 == rec1.intersection(set(nodeset2))):
        if neg_edge == True:
          edge_index.remove(edge_tuple)
          edge = self.get_edge(edge_tuple[0],edge_tuple[1])
          edge.type = 'hard-'

    return edge_index

  # ---------------------------------------------------------------------------
  def sort_edge(self,method,cluster):
    """Method: 0 -> sort random
       Mehtod:  1 -> sort on weight of edge
    """
    edge_index = list()
    index = 0
    if method == 0:
      ''' 
      node_index = cluster.node_list[:]
      for ng in cluster.ng:
        node_group = ng.split("&")
        for node in node_group:
          node_index.remove(node)
        node_index.append(ng)
      random.shuffle(node_index)

      while node_index:

        node = node_index.pop(0)

        for node_2 in node_index:
          if (node,node_2) in cluster.edge_label or (node_2,node) in cluster.edge_label:
            edge = self.get_edge(node,node_2)
            if edge:
              edge_index.append((node,node_2))
              edge.index = index 
              print(edge.weight)
              index +=1 
      '''
      edge_dict = {}
      for rec_tuple in cluster.edge_label:
        edge_dict[rec_tuple] = self.get_edge(rec_tuple[0],rec_tuple[1])
      tedges = sorted(edge_dict.iteritems(), key=lambda d:random.random())
      for e in tedges:
        edge_index.append(e[0])
        print e[1].weight
        e[1].index = index
        index += 1
      
    else:
      edge_dict = {}
      for rec_tuple in cluster.edge_label:
        edge_dict[rec_tuple] = self.get_edge(rec_tuple[0],rec_tuple[1])
      tedges = sorted(edge_dict.iteritems(), key=lambda d:d[1].weight,reverse = True)
      for e in tedges:
        edge_index.append(e[0])
        print e[1].weight
        e[1].index = index
        index += 1
    return edge_index

  # ---------------------------------------------------------------------------
  def get_edge(self,node1,node2):
    """get the edge between two node 
    """
    node1 = self.mnodes.get(node1,node1)
    node2 = self.mnodes.get(node2,node2)
    '''
    if node1 in self.mnodes.keys():
      node1 = self.mnodes[node1]
    if node2 in self.mnodes.keys():
      node2 = self.mnodes[node2]
    '''
    edge = self.edges.get((node1,node2),None)
    if edge:
      return edge
    else:
      return self.edges.get((node2,node1),None)
    '''
    if(node1,node2) in self.edges.keys():
      return self.edges[node1,node2]
    elif (node2,node1) in self.edges.keys():
      return self.edges[node2,node1]
    else:
      return None
    '''
  # ---------------------------------------------------------------------------    
  def find_cluster(self,node):
    """ check whether a node is in a cluster
    """
    node = self.mnodes.get(node,node)
    return self.cluster_dict.get(node,None)
    '''
    if node in self.mnodes.keys():
      node = self.mnodes[node]
    if node in self.cluster_dict.keys():
      return self.cluster_dict[node]
    return None
    '''
  # ---------------------------------------------------------------------------    
  def build_group(self,node1,node2,rule_type):
    """ build group of nodes with + connections 
    """

    #print('build_group')
    cluster_1 = self.find_cluster(node1)
    cluster_2 = self.find_cluster(node2)

    if not cluster_1 and not cluster_2:
      #both records are not in clusters, than build a new cluster
      #print('nc1nc2')

      new_cluster = Cluster.Cluster(node1)
      if rule_type == 'hard+':
        new_cluster.insert_node(node2)
      else:
        new_cluster.insert((node1,node2),node2)
      self.cluster_dict[node1] = new_cluster
      self.cluster_dict[node2] = new_cluster
      #self.clusters.append(new_cluster)
      assert set(node2.split("&")) == set(get_intersection(node2.split("&"),new_cluster.node_list))

       # print new_cluster.node_list,node1,node2
    elif cluster_1 and not cluster_2:
      #print('c1nc2')

      oldsize = len(cluster_1.node_list)
      #add the second record into cluster 1
      if rule_type == 'hard+': 
        cluster_1.insert_node(node2)
      else:
        cluster_1.insert((node1,node2),node2)
      self.cluster_dict[node2] = cluster_1
      assert set(node2.split("&")) == set(get_intersection(node2.split("&"),cluster_1.node_list))

      #  print "cluter1 no2",cluster_1.node_list,node1,node2
    elif cluster_2 and not cluster_1:
      #print('c2nc1')
      oldsize = len(cluster_2.node_list)
      self.cluster_dict[node1] = cluster_2

      if rule_type == 'hard+': 
        cluster_2.insert_node(node1)
      else:
        cluster_2.insert((node1,node2),node1)


    #    print 'cluster2,not1',cluster_2.node_list,node1,node2
    elif cluster_2 != cluster_1:
      #print('c1c2')

      oldsize = len(cluster_1.node_list)
      self.cluster_dict[node2] = cluster_1

      #merge two cluster
      if rule_type == 'hard+':
        cluster_1.insert_node(node2)
      else:

        cluster_1.merge((node1,node2))

      assert set(node2.split("&")) == set(get_intersection(node2.split("&"),cluster_1.node_list))

      for pair_of_record in cluster_2.edge_label:

        cluster_1.merge(pair_of_record)
        self.cluster_dict[pair_of_record[0]] = cluster_1
        self.cluster_dict[pair_of_record[1]] = cluster_1

      #self.clusters.remove(cluster_2)
      try:
        del cluster_2
      except KeyError:
        pass

  def repair_result(self):
    total_size = 0
    size =0
    loop_list = []
    time_list = []
    logging.info('')
    strategy = ''
    #for testing
    if self.repair_method == 0:
      strategy = 'random'
    else:
      strategy = 'weight-based'
    logging.info('Start Repairing using %s strategy' % strategy)   
    self.evaluator.clean_result_list()
    count = 0 
    current_time = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')

    #for rec_tuple,r_type in self.wrong_list.items():
    wrong_key = set((self.wrong_list.keys()))
    start_time = time.clock()
    while self.wrong_list:
      #rec_tuple,r_type = random.choice(self.wrong_list.items())
      rec_tuple = random.choice(tuple(wrong_key))
      wrong_key.remove(rec_tuple)
      r_type = self.wrong_list.pop(rec_tuple)
      #del self.wrong_list[rec_tuple]
      repair_edge = self.get_edge(rec_tuple[0],rec_tuple[1])
      if r_type == "split":
        if repair_edge:
          if repair_edge.type == "hard+":
            self.captureEdge.add_rep_edge(rec_tuple,repair_edge.Init_type,repair_edge.rule_id,1,current_time,"feedback split")
        self.repairing(rec_tuple,'split')
      elif r_type == "merge":
        if repair_edge:
          if repair_edge.type != "hard+" and 'hard' not in repair_edge.Init_type:
            self.captureEdge.add_rep_edge(rec_tuple,repair_edge.Init_type,repair_edge.rule_id,1,current_time,"feedback merge")
        self.repairing(rec_tuple,'merge')

      
      count += 1
      if(count % self.num_repairs) == 0:
        end_time = time.clock()
        time_used = end_time-start_time
        time_list.append(time_used)
        logging.info('The evaluation result of clusters after repairing: %d edges using %d seconds for repairing, total nodes include %d'% (count,time_used,len(self.records)))  
        memory_usage_str = auxiliary.get_memory_usage()
        if (memory_usage_str != None):
          logging.info('    '+memory_usage_str) 
        #self.wrong_list = {}
        self.num_tn = self.num_tp = self.num_fp = self.num_fn = 0
        total_size = 0
        index = 0
        self.clusters = list(set(self.cluster_dict.values()))

        self.wrong_list,size_dict,fn,fp = self.evaluator.get_closest_cluster_measure(self.clusters,False)
        wrong_key = set((self.wrong_list.keys()))
        start_time = time.clock()

        #self.evaluator.get_measures([self.num_tp, self.num_fn, self.num_fp, self.num_tn])     

    total_size = 0
    size = 0
    self.num_tn = self.num_tp = self.num_fp = self.num_fn = 0
    #cluster_list = list(set(self.cluster_dict.values()))
    index = 0
    self.clusters = list(set(self.cluster_dict.values()))
    '''
    for c in self.clusters:
      """debugging
      """
      del cluster_list[index]
      index +=1          
      self.test(c,cluster_list) 
      #self.test(c,cluster_list) 
      #cluster_list.remove(c) 
      size = c.get_size() 
      #print("---------After repairing--------")
      #print(c.node_list)
      #print(c.ng)
      total_size = total_size + size
      if size in size_dict:
        size_dict[size] += 1
      else:
        size_dict[size] = 1    
    '''
    logging.info('Final Reparing result after repair %d edges,Average size of the cluster %.2f' % (count,float(len(self.records))/len(self.clusters)))
    self.wrong_list,size_dict,fn,fp = self.evaluator.get_closest_cluster_measure(self.clusters,True)
    #self.evaluator.get_measures([self.num_tp, self.num_fn, self.num_fp, self.num_tn])     
    self.evaluator.print_measures_list()
    logging.info('Repairing time')
    logging.info(time_list)
    logging.info('Clustering total nodes: %d,Average size of the cluster %.2f' % (len(self.records),float(len(self.records))/len(self.clusters)))
    logging.info('Cluster sizes dict:')
    logging.info(size_dict.keys())
    logging.info(size_dict.values())


    #for k,v in size_dict.items():
    #  logging.info('%d cluster with size of %d' % (v,k))

    logging.info('End of repairing, the number of total repairing: %d, the number of initial errors: %d'%(count,self.initial_errors))
    #self.repairing("merge")
  def repairing(self,rec_tuple,operation):
    """
      repairing the result
    """
    memory_usage = auxiliary.get_memory_usage()
    if memory_usage:
      logging.info(operation+" repair,"+memory_usage)
    #print rec_tuple,operation
    if (operation == "split"):
      self.split(rec_tuple)

    elif(operation == "merge"):
      self.merge(rec_tuple)
    memory_usage = auxiliary.get_memory_usage()
    if memory_usage:
      logging.info(" After repair,"+memory_usage)
  # ---------------------------------------------------------------------------
  def split(self,rec_tuple):
    #change the type of edge
    edge = self.get_edge(rec_tuple[0],rec_tuple[1])
    if edge:
      edge.type = 'hard-'
      edge.Init_type = 'hard-'
    else:
      current_time = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
      edge = Edge.Edge(1,'hard-',0,0,current_time)
    self.add_hard_edge(rec_tuple[0],rec_tuple)
    self.add_hard_edge(rec_tuple[1],rec_tuple)

    cluster = self.find_cluster(rec_tuple[0])
    if(self.find_cluster(rec_tuple[1]) != cluster):
      return
    # Start finding the cut node
    edge = cluster.root

    find_edge = False
    #edge_tuple = None
    remove = None
    cluster1 = Cluster.Cluster(rec_tuple[0])
    cluster2 = Cluster.Cluster(rec_tuple[1])
    while not find_edge:
      children = edge.children
      if children:
        lchild = children[0]
        rchild = children[1]
      else:
        print edge.type
        return
      #print(children)
      if rec_tuple[0] in lchild[0] and rec_tuple[1] in lchild[0]:
          # in the same side , keep tracking
          edge = lchild[1]
          
      elif rec_tuple[0] in rchild[0] and rec_tuple[1] in rchild[0]:
          # in the same side, keep tracking
          edge = rchild[1]
        
      elif (rec_tuple[0] in rchild[0] and rec_tuple[1] in lchild[0]):
          #in two node
          find_edge = True
          #edge_tuple = rchild[1].parent[1]
          cluster1.root = rchild[1]
          cluster1.node_list = rchild[0]
          cluster2.root = lchild[1]
          cluster2.node_list = lchild[0]
      elif  (rec_tuple[1] in rchild[0] and rec_tuple[0] in lchild[0]):
          find_edge = True
          cluster2.root = rchild[1]
          cluster2.node_list = rchild[0]
          cluster1.root = lchild[1]
          cluster1.node_list = lchild[0]
      else:
        print("bug find edge")
        return
    #print("find edge, spliting")
    edge.change_type('hard-')
    #cluster.label.remove_edge(edge_tuple)

    #print(cluster1.node_list)
    #print(cluster2.node_list)
    while edge.parent: 
      parent_edge = edge.parent[0]
      parent_edge_tuple = edge.parent[1]

      #find another side of parent edge
      if set(parent_edge_tuple[0].split("&")) == get_intersection(parent_edge_tuple[0].split("&"),cluster1.node_list) or set(parent_edge_tuple[1].split("&")) == get_intersection(parent_edge_tuple[1].split("&"),cluster1.node_list):
        # set parent of left child as parent edge
        cluster1.root.parent = (parent_edge,parent_edge_tuple)
        if edge == parent_edge.children[0][1]:
          parent_edge.children[0] = (cluster1.node_list,cluster1.root)
          cluster1.node_list = cluster1.node_list + parent_edge.children[1][0]
        elif edge == parent_edge.children[1][1]:
          parent_edge.children[1] = (cluster1.node_list,cluster1.root)
          cluster1.node_list = cluster1.node_list + parent_edge.children[0][0]
        cluster1.root = parent_edge
      elif set(parent_edge_tuple[0].split("&")) == get_intersection(parent_edge_tuple[0].split("&"),cluster2.node_list) or set(parent_edge_tuple[1].split("&")) == get_intersection(parent_edge_tuple[1].split("&"),cluster2.node_list):
        cluster2.root.parent = (parent_edge,parent_edge_tuple)
        if edge == parent_edge.children[0][1]:
          parent_edge.children[0] = (cluster2.node_list,cluster2.root)
          cluster2.node_list = cluster2.node_list + parent_edge.children[1][0]
        elif edge == parent_edge.children[1][1]:
          parent_edge.children[1] = (cluster2.node_list,cluster2.root)
          cluster2.node_list = cluster2.node_list + parent_edge.children[0][0]
        cluster2.root = parent_edge
      else:
        print('bug')
        return
      edge = parent_edge
    #print('get parent')
    #print(cluster1.node_list)
    #print(cluster2.node_list)
    cluster1.root.parent = None
    cluster2.root.parent = None
    for edge_label in cluster.edge_label:
      edge_label_list = edge_label[0].split("&") + edge_label[1].split("&")
      if set(edge_label_list) == get_intersection(edge_label_list,cluster1.node_list):
        cluster1.merge(edge_label)
      elif set(edge_label_list) == get_intersection(edge_label_list,cluster2.node_list):
        cluster2.merge(edge_label)
    for ng in cluster.ng:
      if set(ng.split("&")) == get_intersection(ng.split("&"),cluster1.node_list):
        cluster1.insert_node(ng)
      elif set(ng.split("&")) == get_intersection(ng.split("&"),cluster2.node_list):
        cluster2.insert_node(ng)
    #print(cluster1.node_list,cluster1.ng)
    #print(cluster2.node_list,cluster2.ng)
    for n in cluster1.node_list:
      n =  self.mnodes.get(n,n)
      #if n in self.mnodes.keys():
      #  self.cluster_dict[self.mnodes[n]] = cluster1
      #else:
      self.cluster_dict[n] = cluster1
    for n in cluster2.node_list:
      n = self.mnodes.get(n,n)
      #if n in self.mnodes.keys():
      #  self.cluster_dict[self.mnodes[n]] = cluster2
      #else:
      self.cluster_dict[n] = cluster2
    '''
    if len(cluster.node_list) != len(cluster1.node_list + cluster2.node_list):
      print("error",cluster.node_list)
      print('cluster1',cluster1.node_list)
      print('cluster2',cluster2.node_list)
    '''
    assert len(cluster.node_list) == len(cluster1.node_list + cluster2.node_list)
    #assert len(cluster.edge_label) == len(cluster1.edge_label + cluster2.edge_label)
    #print(cluster.node_list, cluster1.node_list,cluster2.node_list)
    #print(cluster.edge_label,cluster1.edge_label,cluster2.edge_label)
    
    #self.clusters.remove(cluster)
    #self.clusters.append(cluster1)
    #self.clusters.append(cluster2)
    #print('finished')
    #check size
    '''
    total_size = 0
    for cluster in self.clusters:
      total_size += cluster.get_size()
    print('we got %d nodes'% total_size)
    '''
  # ---------------------------------------------------------------------------
  # ---------------------------------------------------------------------------
  def merge(self,rec_tuple):
    """Merge the ER result
    """
    print("++++++++Merge+++++++++")
    edge = self.get_edge(rec_tuple[0],rec_tuple[1])
    if edge:
      edge.Init_type = 'hard+'
      edge.type = "hard+"
    else:
      current_time = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
      self.captureEdge.add_rep_edge(rec_tuple,'hard+',0,1,current_time,"feedback merge")
    clusters_list = []
    #find the cluster to merge
    cluster1 = self.find_cluster(rec_tuple[0])

    cluster2 = self.find_cluster(rec_tuple[1])
    if cluster1 == None or cluster2 == None:
      print('None cluster',rec_tuple)
    if cluster1 == cluster2:
      return

    #find the parent nodes of two leaves
    '''
    node1 = rec_tuple[0]
    node_list1 = [rec_tuple[0]]
    node2 = rec_tuple[1]
    node_list2 = [rec_tuple[1]]
    if rec_tuple[0] in self.mnodes.keys():
      node1 = self.mnodes[rec_tuple[0]]
      node_list1 = node1.split("&")        

    if rec_tuple[1] in self.mnodes.keys():
      node2 = self.mnodes[rec_tuple[1]]
      node_list2 = node2.split("&")
    '''
    node1 = self.mnodes.get(rec_tuple[0],rec_tuple[0])
    node2 = self.mnodes.get(rec_tuple[1],rec_tuple[1])
    node_list1 = node1.split("&")
    node_list2 = node2.split("&")
    nodelist1,edge1,rec_tuple1 = cluster1.find_node(node_list1) 
    nodelist2,edge2,rec_tuple2 = cluster2.find_node(node_list2)
    new_node_name = self.merge_node(node1,node2)
    #print(new_node_name)

    #debug
    #print(edge1.children)
    #print(edge2.children)
    '''
    print(node1,rec_tuple1)
    print(node2,rec_tuple2)
    '''
    node = new_node_name.split("&")
    ldistance = 0
    rdistance = 0
    new_cluster = Cluster.Cluster(new_node_name)
    new_cluster.insert_node(new_node_name)
    clusters_list.append(new_cluster)
    if rec_tuple1 and rec_tuple2:
      edge_list = [(edge1,rec_tuple1,cluster1),(edge2,rec_tuple2,cluster2)]
    elif rec_tuple1:
      edge_list = [(edge1,rec_tuple1,cluster1)]
    elif rec_tuple2:
      edge_list = [(edge2,rec_tuple2,cluster2)]
    else:
      # one node 
      cluster1.insert_node(node2)

      #self.clusters.remove(cluster2)
      del cluster2
      return
    print("++++++++list+++++++++")

    last_edge = Edge.Edge(0,'node',0,-1,None)
    new_cluster.root = last_edge
    print("++++++++tree+++++++++")

    if self.repair_method == 0:  # edges is picked in random order
      while edge_list:

        if len(edge_list) == 1:
          cluster = self.merge_braches(edge_list[0][0],edge_list[0][1],edge_list[0][2],clusters_list)
          #last_edge = edge_list[0][0]
          edge = edge_list[0][0]
          rec_tuple = edge_list[0][1]
          parent =  edge_list[0][0].parent
          cluster_p = edge_list[0][2]
          del edge_list[0]
          #last_edge = edge_list.pop(0)[0]
          if parent:
            edge_list.insert(0,(parent[0],parent[1],cluster_p))  
          if cluster:
            clusters_list.append(cluster)
            self.del_edge(rec_tuple)
            del edge

        else:
          if ldistance == rdistance:
            ran = bool(random.getrandbits(1))
            if ran:
              ldistance  = rdistance -1
            else:
              rdistance = ldistance -1
          if ldistance < rdistance:
            cluster = self.merge_braches(edge_list[0][0],edge_list[0][1],edge_list[0][2],clusters_list)
            edge = edge_list[0][0]
            rec_tuple = edge_list[0][1]

            parent = edge_list[0][0].parent
            del edge_list[0]
            #last_edge = edge_list[0][0]
            #edge_list.pop(0)
            #last_edge = edge_list.pop(0)[0]
            if parent:
              edge_list.insert(0,(parent[0],parent[1],cluster1))
              ldistance +=1
            #last_edge = edge_list.pop(0)[0]

          else:
            cluster = self.merge_braches(edge_list[1][0],edge_list[1][1],edge_list[1][2],clusters_list)
            edge = edge_list[1][0]
            rec_tuple = edge_list[1][1]
            parent = edge_list[1][0].parent
            #last_edge = edge_list[1][0]
            #edge_list.pop(1)
            del edge_list[1]
            #last_edge = edge_list.pop(1)[0]
            if parent:
              edge_list.insert(1,(parent[0],parent[1],cluster2))
              rdistance+=1
            #last_edge = edge_list.pop(0)[0]

          if cluster:
            clusters_list.append(cluster)
            self.del_edge(rec_tuple)
            del edge

    else:
      #sort on weight
      while edge_list:
        if len(edge_list) == 1:
          cluster = self.merge_braches(edge_list[0][0],edge_list[0][1],edge_list[0][2],clusters_list)
          #last_edge = edge_list[0][0]
          edge = edge_list[0][0]
          rec_tuple = edge_list[0][1]
          parent =  edge_list[0][0].parent
          cluster_p = edge_list[0][2]
          del edge_list[0]
          #last_edge = edge_list.pop(0)[0]
          if parent:
            edge_list.insert(0,(parent[0],parent[1],cluster_p))  
          if cluster:
            clusters_list.append(cluster)
            self.del_edge(rec_tuple)
            del edge
        else:
          if edge_list[0][0].weight <= edge_list[1][0].weight:
            cluster = self.merge_braches(edge_list[0][0],edge_list[0][1],edge_list[0][2],clusters_list)
            edge = edge_list[0][0]
            rec_tuple = edge_list[0][1]

            parent = edge_list[0][0].parent
            del edge_list[0]
            #last_edge = edge_list[0][0]
            #edge_list.pop(0)
            #last_edge = edge_list.pop(0)[0]
            if parent:
              edge_list.insert(0,(parent[0],parent[1],cluster1))
            #last_edge = edge_list.pop(0)[0]
          else:
            cluster = self.merge_braches(edge_list[1][0],edge_list[1][1],edge_list[1][2],clusters_list)
            edge = edge_list[1][0]
            rec_tuple = edge_list[1][1]
            parent = edge_list[1][0].parent
            #last_edge = edge_list[1][0]
            #edge_list.pop(1)
            del edge_list[1]
            #last_edge = edge_list.pop(1)[0]
            if parent:
              edge_list.insert(1,(parent[0],parent[1],cluster2))
            #last_edge = edge_list.pop(0)[0]

          if cluster:
            clusters_list.append(cluster)
            self.del_edge(rec_tuple)
            del edge

    # ToDO:
    #label to each cluster
    edge_labels = cluster1.edge_label+cluster2.edge_label
    edge_label_copy = edge_labels[:]
    total_nodes1 = cluster1.get_size() + cluster2.get_size()
    #print(cluster1.ng)
    #print(cluster2.ng)
    total_node = 0

    for ng in (cluster1.ng + cluster2.ng):
      for c in clusters_list:
        if set(ng.split("&")) == get_intersection(ng.split("&"),c.node_list):
          c.insert_node(ng)
    while edge_label_copy:
      edge_label = edge_label_copy.pop()
      edge_label_list = edge_label[0].split("&") + edge_label[1].split("&")
      for c in clusters_list:
        if set(edge_label_list) == set(get_intersection(edge_label_list,c.node_list)):
          c.merge(edge_label)

    for c in clusters_list:
      c.root.parent = None
      #self.clusters.append(c)
      total_node += c.get_size()
      for n in c.node_list:
        n =  self.mnodes.get(n,n)
        self.cluster_dict[n] = c
        '''
        if n in self.mnodes.keys():
          self.cluster_dict[self.mnodes[n]] = c
        else:
          self.cluster_dict[n] = c
        '''
      #print(c.node_list)
      #print(c.ng)
    if total_node != total_nodes1:
      print("------Merge Error -------")
      print cluster1.node_list,cluster2.node_list
      for c in clusters_list:
        print c.node_list
    #self.clusters.remove(cluster1)
    #self.clusters.remove(cluster2)
    
    total_size = 0
    '''
    for cluster in self.clusters:
      total_size += cluster.get_size()
    print('we got %d nodes'% total_size)
    '''
    del cluster1
    del cluster2
    del edge_labels

    #debugging
    '''
    print("______result________")
    for cluster in clusters_list:
      print(cluster.node_list)
      self.clusters.append(cluster)
    for cluster in self.clusters:
      print(cluster.node_list)
      print(cluster.edge_label)
      print(cluster.ng)
    
    '''
    '''
      edge = cluster.root
      print("______result________chekcing!")
      while edge.children != []:
        if(edge.parent):
          print(edge.parent)
        else:
          print("none parent",edge)
        print(edge.children)
        print(edge.children[0][1])
        print(edge.children[1][1])
        edge = edge.children[0][1]
    '''
    '''
    for cluster in self.clusters:
      print("root child",cluster.root.children)
      print("--node list",cluster.node_list)
      print("--ng",cluster.ng)
      print("--edge lable",cluster.edge_label)
    '''
  def merge_braches(self,edge,rec_tuple,previouscluster,clusters_list):
    #check if has hard neg rules between two side
    #print 'node',node 
    #print 'rec_tuple',rec_tuple
    print("++++++++branches+++++++++")

    children = edge.children
    cluster = None
    rec_list = rec_tuple[0].split("&") + rec_tuple[1].split("&")
    #print("rec_list",rec_list)
    for c in clusters_list:
      
      #print "node_list",c.node_list
      if list(get_intersection(rec_list,c.node_list))!=[]:
        #print("In node")
        #print(children[0][0],children[1][0])
        #print( list(get_intersection(c.node_list,children[0][0])), list(get_intersection(c.node_list,children[1][0])))
        if list(get_intersection(c.node_list,children[0][0]))!=[]:
          lchild = children[0]
          rchild = children[1]    
          cluster = c 

        elif list(get_intersection(c.node_list,children[1][0]))!=[]:
          lchild = children[1]
          rchild = children[0] 
          cluster = c

    '''

    print("before",node)
    print("merge_edge",rec_tuple)
    print(children)
    '''
    if (cluster == None):
      print("bugging")
      return
    node = cluster.node_list
    last_edge = cluster.root
    for n in node:
      if n in self.hard_neg_edges.keys():
        neg_edges = self.hard_neg_edges[n]
        for ne in neg_edges:
          if get_intersection(list(ne),rchild[0]) != set([]):
            #print(previouscluster.edge_label)
            #print previouscluster.node_list
            #print(edge.children)

            record = list(get_intersection(list(ne),rchild[0]))[0]
            #print(record)
            new_cluster = Cluster.Cluster(record)
            new_cluster.root = rchild[1]
            new_cluster.node_list = rchild[0]
            rchild[1].parent = None
            #print(new_cluster.node_list)
            #print("new cluster!!!!")
            #find root of the new cluster
            return new_cluster

    node_list = node + rchild[0]

    cluster.node_list = node_list

    cluster.merge(rec_tuple)
    edge.children[0] = node,last_edge
    edge.children[1] = rchild
    #lchild[1].parent = None
    last_edge.parent = (edge,rec_tuple)
    del lchild
    #print("children after merge",edge.children)
    cluster.root = edge


    return None
  # ---------------------------------------------------------------------------

  # ---------------------------------------------------------------------------

  def count_edge_types(self):
    hardp = 0
    hardn = 0
    softp = 0
    logging.info('')
    logging.info("Clustering results:")
    for key,edge in self.edges.items():
      if edge == None:
        print key
      if edge.type == 'hard+':
        hardp+=1
      elif edge.type == 'hard-':
        hardn+=1
      elif edge.type == 'soft+':
        softp+=1
        
    logging.info("Hard+:%d, Hard-:%d, Soft+:%d"%(hardp,hardn,softp))

  # ---------------------------------------------------------------------------
  def test(self,cluster,clusters_list):
    """ 
    """
    tp = 0
    fp = 0
    tn = 0
    fn = 0
    cluster.tree_checking()
    for node_1 in cluster.node_list:
      for node_2 in cluster.node_list:
        if(self.evaluator.evalute((node_1,node_2))):
          tp += 1
        else:
          #self.log([('FP', rec_id_tuple)])
          edge = self.get_edge(node_1,node_2)
          if edge and edge.Init_type == 'hard+':
            #logging.info("Error hard+ edge ")
            print((node_1,node_2),edge.weight)

          else:
            if (node_1,node_2) not in self.wrong_list and (node_2,node_1) not in self.wrong_list:
              self.wrong_list[(node_1,node_2)] = 'split'
          fp += 1
      for cluster_2 in clusters_list:
        for node_2 in cluster_2.node_list:
          if(self.evaluator.evalute((node_1,node_2))):
            fn += 1 
            edge = self.get_edge(node_1,node_2)
            if edge and edge.Init_type == "hard-":
              logging.info("Error hard- edge")
            else:
              if (node_1,node_2) not in self.wrong_list and (node_2,node_1) not in self.wrong_list:
                self.wrong_list[(node_1,node_2)] = 'merge'
          else:
            tn += 1           
    self.num_tp += tp
    self.num_fp += fp
    self.num_fn += fn
    self.num_tn += tn
  # ---------------------------------------------------------------------------
  def build_single_cluster(self,rec_tuple):
    '''
      build cluster only has one node
    '''
    for rec in rec_tuple:
      self.records.add(rec)
      c = self.find_cluster(rec)

      if not c:
        cluster = Cluster.Cluster(rec)
        cluster.root = Edge.Edge(0,'node',0,-1,None)
        #self.clusters.append(cluster)
        self.cluster_dict[rec] = cluster

# ---------------------------------------------------------------------------
def get_intersection(list1,list2):
  return set(list1).intersection(set(list2)) 
