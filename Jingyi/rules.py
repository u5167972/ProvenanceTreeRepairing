# =============================================================================
# Import necessary modules
import csv
import logging
import math
import os
import random
import shelve
import string
import sys
import time
from collections import deque

sys.path.append('..')

import auxiliary


# =============================================================================
class Rules:
  """ The class to read rule file and instore rules
  """
  # ---------------------------------------------------------------------------

  def __init__(self, **kwargs):
    """Constructor, set general attributes.
    """
    # General attributes
    #
    self.file_name =        None   # The name of the CSV file
    self.file =             None   # File pointer to current file
    self.write_quote_char = ''     # The quote character for writing fields
    self.delimiter  =       ','    # The delimiter character
    self.exists =           False
    self.rid =              0
    for (keyword, value) in kwargs.items():

      if (keyword.startswith('file')):
        auxiliary.check_is_string('file_name', value)
        self.file_name = value

      elif (keyword.startswith('write_qu')):
        auxiliary.check_is_string('write_quote_char', value)
        self.write_quote_char = value
      elif (keyword.startswith('delimi')):
        auxiliary.check_is_string('delimiter', value)
        if (len(value) != 1):
          logging.exception('Value of "delimiter" argument must be a one-' + \
                            'character string, but it is: "%s"' % (delimiter))
          raise Exception
        self.delimiter = value

  # ---------------------------------------------------------------------------

  def finalise(self):
    """Finalise a data set, i.e. close the file and set various attributes to
       None.
    """

    # Close file if it is open
    #
    if (self.file != None):

      self.file.close()
      self.file = None

    self.access_mode =  None
    self.file_name =    None
    self.num_records =  None
    self.next_rec_num = None

    # A log message - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    #
    logging.info('Finalised CSV data set "%s"' % (self.description))

  # ---------------------------------------------------------------------------

  def add_rule(self,rule,r_type,r_weight,dataset):
    """Write the rule into the CSV file.
    """
    if(os.path.exists(self.file_name)):
      repeat = self.check_repeat(rule)
      if repeat:
        self.rid = int(repeat)
        return self.rid
    if (self.file != None):
      self.file.close()  # Close currently open file
    if(os.path.exists(self.file_name)):
      try:
      	self.file = open(self.file_name,'rb+')
        self.exists = True
      except:
        logging.exception('Cannot open CSV file "%s" for appending' % \
                            (self.file_name))
    else:
      try:
        self.file = open(self.file_name,'wb+')
        self.exists = False
      except:
        logging.exception('Cannot open CSV file "%s" for writing' % \
                          (self.file_name))
    self.csv_parser = csv.writer(self.file, delimiter = self.delimiter)
    try:
      last_line = deque(csv.reader(self.file, delimiter = self.delimiter), 1)[0]
      self.rid = int(last_line[0]) + 1
    except:
      self.rid = 1
    rule_tw = [self.rid,rule,r_type,r_weight,dataset]
    if (self.write_quote_char != ''):
      rule_tw = map(lambda s:self.write_quote_char+s+self.write_quote_char, rule_tw)
    self.csv_parser.writerow(rule_tw)  # Write rule to file
    self.file.flush()
    self.file.close()
    return self.rid
  def check_repeat(self,rule_str):
    """Check whether the same rule exist in the file
    """
    rid = False
    if (self.file != None):
      self.file.close()  # Close currently open file
    try:
      self.file = open(self.file_name,'rb')
      self.csv_parser = csv.reader(self.file, delimiter = self.delimiter)
    except:
      logging.exception('Cannot open CSV file "%s" for reading' % \
                          (self.file_name))    

    self.csv_parser = csv.reader(self.file, delimiter = self.delimiter)
    for rule in self.csv_parser:
      rule = map(string.strip, rule)
      if rule[1] == rule_str:
        rid = rule[0]
    self.file.close()
    return rid

  def get_rule(self,rule_ids):
    """ get the rule from csv file
    """
    if (self.file != None):
      self.file.close()  # Close currently open file
    try:
      self.file = open(self.file_name,'rb')
      self.csv_parser = csv.reader(self.file, delimiter = self.delimiter)
    except:
      logging.exception('Cannot open CSV file "%s" for reading' % \
                          (self.file_name))    

    self.csv_parser = csv.reader(self.file, delimiter = self.delimiter)
    rules_list = []
    for rule in self.csv_parser:
      rule = map(string.strip, rule)
      if int(rule[0]) in rule_ids:
        rules_list.append(rule)
    self.file.close()
    return rules_list
# =============================================================================